#!/bin/sh

OUTPUT_FILENAME=rasterizer

if [ "$1" = "noopt" ]; then
    OUTPUT_FILENAME=rasterizer_noopt
fi

cd rasterizer && ../output/$OUTPUT_FILENAME
