#include <fstream>
#include <string>
#include <cstring>

using namespace std;

void genpfn( string cur, char *pfnstr )
{
    strcpy( pfnstr, "PFN" );
    for( int i = 0; i < ( int )cur.length( ); i++ ) {
        pfnstr[i+3] = toupper( cur[i] );
    }
    pfnstr[3 + cur.length( )] = 0;
    strcat( pfnstr, "PROC" );
}

int main( )
{
    ifstream in( "..\\SDL\\include\\SDL_opengl.h" );
    ofstream outp( "prototypes.txt" ), outl( "load.txt" ), outex( "extern.txt" );;
    string cur;
    char pfnstr[111111];
    int cnt = 0;
    bool f = false;

    while( in >> cur ) {
        if( cur == "/*" ) {
            f = true;
        }
        if( cur == "GL_GLEXT_PROTOTYPES" ) {
            if( f ) {
                f = false;
                continue;
            }
            while( cur != "#endif" ) {
                in >> cur;
                if( cur[0] == 'g' && cur[1] == 'l' ) {
                    genpfn( cur, pfnstr );
                    outp << pfnstr << " _" << cur << ";" << endl;
                    outex << "extern " << pfnstr << " _" << cur << ";" << endl;
                    outl << "_" << cur << " = " << "( " << pfnstr << " )" << "SDL_GL_GetProcAddress( \"" << cur << "\" );" << endl;
                }
            }
        }
    }

    in.close( );
    in.open( "GL.h" );
    ofstream outdef( "typedefs.txt" );
    outp << endl << "GL 1.1 prots" << endl << endl;
    outl << endl << "GL 1.1 funcs" << endl << endl;

    while( in >> cur ) {
        if( cur == "WINGDIAPI" ) {
            string rettype;
            in >> rettype;
            while( 1 ) {
                in >> cur;
                if( cur[0] == 'g' && cur[1] == 'l' ) {
                    genpfn( cur, pfnstr );
                    outdef << "typedef " << rettype << " ( APIENTRY * " << pfnstr << " )";
                    outp << pfnstr << " _" << cur << ";" << endl;
                    outex << "extern " << pfnstr << " _" << cur << ";" << endl;
                    outl << "_" << cur << " = " << "( " << pfnstr << " )" << "SDL_GL_GetProcAddress( \"" << cur << "\" );" << endl;
                    while( 1 ) {
                        in >> cur;
                        outdef << cur;
                        if( cur.back( ) == ';' ) {
                            break;
                        } else {
                            outdef << " ";
                        }
                    }
                    outdef << endl;
                    break;
                }
            }
        }
    }
    
    return 0;
}