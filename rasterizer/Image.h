#pragma once

namespace Rasterizer2
{
    enum PixelFormat
    {
        PIXEL_R8G8B8,
        PIXEL_R8G8B8A8
    };

    class Image
    {

    public:

        ~Image( );

        bool LoadArbitraryImage( const char *path );
        void GetData( byte *outBuf, PixelFormat pf );
        INLINE int GetWidth( )
        {
            return m_width;
        }
        INLINE int GetHeight( )
        {
            return m_height;
        }

        static int GetPixelFormatSize( PixelFormat fmt );

    private:

        ILuint m_image;
        int m_width;
        int m_height;

    };
}
