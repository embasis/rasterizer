#include "precompiled.h"
#include "MTLFile.h"
#include "Texture.h"

namespace Rasterizer2
{
    MTLFile::MTLFile( IState *state ) : m_state( state )
    {
    }

    MTLFile::~MTLFile( )
    {
        Clear( );
    }

    void MTLFile::Clear( )
    {
        for( Material *m : m_materials ) {
            delete m;
        }
        m_materials.clear( );
    }

    bool MTLFile::Contains( const char *matName ) const
    {
        for( Material *mat : m_materials ) {
            if( !strcmp( mat->name, matName ) ) {
                return true;
            }
        }
        return false;
    }

    bool MTLFile::CreateMaterial( const char *matName, Rasterizer2::Material &dest ) const
    {
        for( Material *mat : m_materials ) {
            if( !strcmp( mat->name, matName ) ) {
                if( *mat->diffuseMap ) {
                    Image img;
                    char fullPath[MAX_PATH];
                    strcpy( fullPath, GetFileName( ) );
                    strcat( fullPath, "/" );
                    strcat( fullPath, mat->diffuseMap );
                    if( !img.LoadArbitraryImage( fullPath ) ) {
                        return false;
                    }
                    Texture *tex = m_state->GetTexturesManager( ).Create( ); // leak you say? don't care
                    tex->CreateFromImage( img, PIXEL_R8G8B8A8, false );
                    dest.SetDefaults( );
                    dest.SetTextureMap( Rasterizer2::Material::MAP_DIFFUSE, tex );
                }
                return true;
            }
        }
        
        DebugOutput::Printf( "\"%s\" material name does not exist!\n", matName );

        return false;
    }

    int MTLFile::GetMaterialsCount( ) const
    {
        return ( int )m_materials.size( );
    }

    bool MTLFile::Parse( FILE *file )
    {
        const int MAX_STR_LENGTH = 2047, MAX_TOKENS = MAX_STR_LENGTH / 2 + 2;
        char str[MAX_STR_LENGTH+1];

        Clear( );

        std::unique_ptr< Material > curMat;

        for( int line = 1; fgets( str, MAX_STR_LENGTH + 1, file ); ++line ) {
            // delete \n \r \t symbols
            for( int i = 0; str[i]; ++i ) {
                if( str[i] == '\n' || str[i] == '\r' || str[i] == '\t' ) {
                    str[i] = ' ';
                }
            }

            // skip comments
            if( *str == '#' ) {
                continue;
            }

            char *tokens[MAX_TOKENS];
            int tokenCnt = 0;

            tokens[0] = strtok( str, " " );
            while( tokens[tokenCnt] != 0 ) {
                tokens[++tokenCnt] = strtok( 0, " " );
            }

            if( tokenCnt ) {
                if( !strcmp( tokens[0], "newmtl" ) ) {
                    if( !( tokenCnt - 1 ) ) {
                        DebugOutput::Printf( "line %d: expected 1 token, 0 found\n", line );
                        return false;
                    }
                    if( strlen( tokens[1] ) > MAX_MATERIAL_NAME_LENGTH ) {
                        DebugOutput::Printf( "line %d: \"%s\" material name is too long\n", line, tokens[1] );
                        return false;
                    }
                    if( curMat ) {
                        m_materials.push_back( curMat.release( ) );
                    }
                    curMat = std::unique_ptr< Material >( new Material( ) );
                    strcpy( curMat->name, tokens[1] );
                } else {
                    if( !curMat ) {
                        DebugOutput::Printf( "line %d: newmtl must be specified before any other statements!\n", line );
                        return false;
                    }
                    if( !strcmp( tokens[0], "Ka" ) || !strcmp( tokens[0], "Kd" ) || !strcmp( tokens[0], "Ks" ) || !strcmp( tokens[0], "Tf" ) ) {
                        if( tokenCnt - 1 < 3 ) {
                            DebugOutput::Printf( "line %d: expected at least 3 tokens, %d found\n", line, tokenCnt - 1 );
                            return false;
                        }
                        if( !strcmp( tokens[1], "spectral" ) || !strcmp( tokens[1], "xyz" ) ) {
                            DebugOutput::Printf( "line %d: \"%s\" variation of \"%s\" statement is unsupported, ignored\n", line, tokens[1], tokens[0] );
                        } else {
                            Vec4 *dest;
                            if( !strcmp( tokens[0], "Ka" ) ) {
                                dest = &curMat->ambientColor;
                            } else if( !strcmp( tokens[0], "Kd" ) ) {
                                dest = &curMat->diffuseColor;
                            } else if( !strcmp( tokens[0], "Ks" ) ) {
                                dest = &curMat->specularColor;
                            } else {
                                dest = &curMat->transmissionFilter;
                            }
                            int read = 0;
                            for( int i = 0; i < 3; ++i ) {
                                read += sscanf( tokens[i + 1], "%f", &(*dest)[i] );
                            }
                            if( read < 3 ) {
                                DebugOutput::Printf( "line %d: expected 3 floats, %d found\n", line, read );
                                return false;
                            }
                        }
                    } else if( strstr( tokens[0], "map_" ) ) {
                        if( !( tokenCnt - 1 ) ) {
                            DebugOutput::Printf( "line %d: expected at least 1 token, 0 found\n", line );
                        }
                        if( strstr( tokens[0], "Kd" ) ) {
                            // skip all parameters for now
                            int curToken = 1;
                            while( *tokens[curToken] == '-' ) {
                                ++curToken;
                            }
                            // texture file path can contain spaces, so we need to directly loop over string characters
                            // strtok replaced spaces with zero bytes and now we need to perform the exact opposite operation
                            int lastTokenLen = strlen( tokens[tokenCnt - 1] );
                            for( char *itk = tokens[curToken]; itk != tokens[tokenCnt - 1] + lastTokenLen; ++itk ) {
                                if( !*itk ) {
                                    *itk = ' ';
                                }
                            } 
                            if( strlen( tokens[curToken] ) > MAX_MAP_PATH_LENGTH ) {
                                DebugOutput::Printf( "line %d: map path \"%s\" is too long\n", line, tokens[curToken] );
                                return false;
                            }
                            strcpy( curMat->diffuseMap, tokens[curToken] );
                        } else {
                            DebugOutput::Printf( "line %d: unsupported \"map\" statement type \"%s\" found, ignored\n", line, tokens[0] );
                        }
                    } else {
                        DebugOutput::Printf( "line %d: unrecognized keyword \"%s\", ignored\n", line, tokens[0] );
                    }
                }
            }
        }

        if( curMat ) {
            m_materials.push_back( curMat.release( ) );
        }

        return true;
    }

    bool MTLFile::ParseFromMemory( const void *buffer )
    {
        return false;
    }

    const char *MTLFile::GetFileType( ) const
    {
        return "MTLFile";
    }

    const char *MTLFile::GetFileLocation( ) const
    {
        return GEOMETRY_LOCATION;
    }
}