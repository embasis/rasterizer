#pragma once

#include "MemoryPool.h"
#include "Script.h"

namespace Rasterizer2
{
    class ScriptEngine
    {

    public:

        static const int SLOT_CNT = 10;
        static const int STRING_POOL_SIZE = 8 * 1024 * 1024; // 8 megs

        class SlotManager
        {

        public:

            INLINE SlotManager( MemoryPool &stringPool ) : m_depth( 0 ), m_stringPool( stringPool )
            {
                m_slotPool.Initialize( ( sizeof( Slot ) + MemoryPool::GetBlockSize( ) ) * SLOT_CNT * 32 );
                DebugOutput::Printf( "ScriptEngine::SlotManager slot pool was initialized\n" );

                Slot *rootSlots = m_slotPool.Allocate( sizeof( Slot ) * SLOT_CNT );
                for( int i = 0; i < SLOT_CNT; ++i ) {
                    rootSlots[i].value.char_ptr = nullptr;
                    rootSlots[i].type = StateCmd::ArgDesc::STRING;
                    rootSlots[i].next = nullptr;
                    rootSlots[i].depth = 0;
                    m_slots[i] = rootSlots + i;
                    m_rootSlots[i] = rootSlots + i;
                }
            }

            INLINE ~SlotManager( ) { };

            INLINE void Reset( )
            {
                m_depth = 0;
                m_slots = m_rootSlots;
                for( int i = 0; i < SLOT_CNT; ++i ) {
                    m_slots[i]->value.char_ptr = nullptr;
                    m_slots[i]->type = StateCmd::ArgDesc::STRING;
                }
            }
            INLINE void IncreaseDepth( )
            {
                ++m_depth;
            }
            INLINE void DecreaseDepth( )
            {
                if( m_depth > 0 ) {
                    for( int i = 0; i < SLOT_CNT; ++i ) {
                        if( m_slots[i]->depth == m_depth ) {
                            m_slots[i] = m_slots[i]->prev;
                        }
                    }
                    --m_depth;
                }
            }
            INLINE StateCmd::ArgBasic ReadSlotValue( int slotNum ) const
            {
                return m_slots[slotNum]->value;
            }
            INLINE StateCmd::ArgDesc::Type ReadSlotType( int slotNum ) const
            {
                return m_slots[slotNum]->type;
            }
            INLINE void WriteSlot( int slotNum, StateCmd::ArgBasic value, StateCmd::ArgDesc::Type type )
            {
#ifdef _DEBUG
                assert( m_slots[slotNum]->depth <= m_depth );
#endif
                if( m_slots[slotNum]->depth < m_depth ) {
                    if( !m_slots[slotNum]->next ) {
                        m_slots[slotNum]->next = ( Slot* )m_slotPool.Allocate( sizeof( Slot ) );
                        m_slots[slotNum]->next->prev = m_slots[slotNum];
                        m_slots[slotNum]->next->next = nullptr;
                    }
                    m_slots[slotNum] = m_slots[slotNum]->next;
                    m_slots[slotNum]->depth = m_depth;
                }

                m_slots[slotNum]->value = value;
                m_slots[slotNum]->type = type;
            }

        private:

            struct Slot
            {
                StateCmd::ArgBasic value;
                StateCmd::ArgDesc::Type type;
                int depth;
                Slot *next;
                Slot *prev;
            };

            int m_depth;
            MemoryPool &m_stringPool;
            MemoryPool m_slotPool;
            Slot *m_slots[SLOT_CNT];
            Slot *m_rootSlots[SLOT_CNT];

        };

        ScriptEngine( );
        ~ScriptEngine( );

        void Execute( Script *script, const char **argv, int argc );
        void Execute( const std::vector< Script::Command > &commands, const char **argv, int argc );

        INLINE const Script *GetRootScript( ) const
        {
            return m_rootScript;
        }
        INLINE const Script *GetCurrentScript( ) const
        {
            return m_curScript;
        }

    private:

        MemoryPool m_stringPool;
        SlotManager m_slots;
        Script *m_rootScript;
        Script *m_curScript;
    };
}