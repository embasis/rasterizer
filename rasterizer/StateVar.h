#pragma once

namespace Rasterizer2
{
    class StateVar
    {

    public:

        static const int MAX_NAME_LENGTH = 64;
        static const int MAX_DESCRIPTION_LENGTH = 256;
        static const int MAX_STRING_VALUE_LENGTH = 512;

        enum Type {
            INTEGER = 1,
            FLOAT,
            STRING
        };

        enum Flags {
            F_READABLE = 1, // user can read the state var
            F_WRITABLE = BIT_FLAG( 1 ), // user can write to the state var
            F_CONSTANT = BIT_FLAG( 2 ) // the var cannot be change after creation, even from the code
        };

        StateVar( const char *name, const char *description, const char *strValue, uint flags, const char **validValues = nullptr, int validValsCnt = 0, Type type = Type::STRING );
        StateVar( const char *name, const char *description, int intValue, uint flags, int *validValues = nullptr, int validValsCnt = 0, int minValue = std::numeric_limits<int>::min( ), int maxValue = std::numeric_limits<int>::max( ) );
        StateVar( const char *name, const char *description, float floatValue, uint flags, float *validValues = nullptr, int validValsCnt = 0, float minValue = std::numeric_limits<float>::min( ), float maxValue = std::numeric_limits<float>::max( ) );

        INLINE const char *GetName( ) const
        {
            return m_name;
        }

        INLINE const char *GetDescription( ) const
        {
            return m_description;
        }

        INLINE const char *GetStringValue( ) const
        {
            return m_strValue;
        }

        INLINE int GetIntValue( ) const
        {
            return m_intValue;
        }

        INLINE float GetFloatValue( ) const
        {
            return m_floatValue;
        }

        INLINE bool isReadable( ) const
        {
            return m_flags & F_READABLE;
        }

        INLINE bool isWritable( ) const
        {
            return m_flags & F_WRITABLE;
        }

        INLINE bool isConstant( ) const
        {
            return m_flags & F_CONSTANT;
        }

        void SetStringValue( const char *strValue );
        void SetIntValue( int intValue );
        void SetFloatValue( float floatValue );

    private:

        StateVar( const char *name, const char *description, uint flags, Type type );

        void SetDefaults( );

        char m_name[MAX_NAME_LENGTH+1];
        char m_description[MAX_DESCRIPTION_LENGTH+1];
        char m_strValue[MAX_STRING_VALUE_LENGTH+1];
        int m_intValue;
        float m_floatValue;
        char m_defaultStrValue[MAX_STRING_VALUE_LENGTH+1];
        int m_defaultIntValue;
        float m_defaultFloatValue;
        std::vector< std::string > m_strValidValues;
        std::vector< int > m_intValidValues;
        std::vector< float > m_floatValidValues;
        int m_intMinValue;
        int m_intMaxValue;
        float m_floatMinValue;
        float m_floatMaxValue;
        uint m_flags;
        Type m_type;

    };
}