#pragma once

#include "File.h"
#include "Mesh.h"
#include "MTLFile.h"

namespace Rasterizer2
{
    class OBJFile : public File
    {

    public:

        OBJFile( IState *state );
        ~OBJFile( );

        void Clear( );
        Mesh *MakeMesh( ) const;

    private:

        struct Vertex 
        {
            INLINE Vertex( ) : posIndex( 0 ), uvIndex( 0 ), normalIndex( 0 )
            {
            }
            int posIndex;
            int uvIndex;
            int normalIndex;

            INLINE bool operator<( const Vertex &v ) const
            {
                return posIndex == v.posIndex ? ( uvIndex == v.uvIndex ? normalIndex < v.normalIndex : uvIndex < v.uvIndex ) 
                                              : posIndex < v.posIndex;
            }
        };

        struct Face 
        {
            INLINE Face( ) : vertIndex( -1 ), vertCnt( 0 ), smoothingGroup( 0 )
            {
                *materialName = 0;
            }
            int vertIndex;
            int vertCnt;
            int smoothingGroup;
            char materialName[MTLFile::MAX_MATERIAL_NAME_LENGTH+1];
        };

        IState *m_state;

        std::vector< Vec3 > m_pos;
        std::vector< Vec3 > m_uv;
        std::vector< Vec3 > m_normal;
        std::vector< Vertex > m_vertexes;
        std::vector< MTLFile* > m_mtl;
        std::vector< Face* > m_faces;

        const char *GetFileType( ) const;
        const char *GetFileLocation( ) const;

        bool Parse( FILE *file );
        bool ParseFromMemory( const void *buffer );

    };
}