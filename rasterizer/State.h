#pragma once

#include "Rasterizer.h"
#include "DemoScene.h"
#include "ICommandSystem.h"
#include "IState.h"

namespace Rasterizer2
{
    class State : public IState
    {

    public:

        State( );
        ~State( );

        bool LaunchDemoScene( );
        void SetupCommandSystem( ICommandSystem *cmdSystem );

        INLINE bool Frame( float timeDelta )
        {
            return m_scene.Frame( timeDelta );
        }
        INLINE void UpdateScreen( )
        {
            return m_scene.UpdateScreen( );
        }
        ResourceHolder< Texture > &GetTexturesManager( )
        {
            return m_texturesManager;
        }
        const ResourceHolder< Texture > &GetTexturesManager( ) const
        {
            return m_texturesManager;
        }
        ResourceHolder< Mesh > &GetMeshesManager( )
        {
            return m_meshesManager;
        }
        const ResourceHolder< Mesh > &GetMeshesManager( ) const
        {
            return m_meshesManager;
        }
        ResourceHolder< Script > &GetScriptsManager( )
        {
            return m_scriptsManager;
        }
        const ResourceHolder< Script > &GetScriptsManager( ) const
        {
            return m_scriptsManager;
        }

    private:

        bool InitRasterizer( );

        Rasterizer m_r;
        DemoScene m_scene;
        ResourceHolder< Texture > m_texturesManager;
        ResourceHolder< Mesh > m_meshesManager;
        ResourceHolder< Script > m_scriptsManager;

    };
}