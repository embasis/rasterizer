#pragma once

#include "Rasterizer.h"
#include "Scene.h"

namespace Rasterizer2
{
    class DemoScene
    {
        
    public:

        DemoScene( Rasterizer &r, IState *state );
        ~DemoScene( );

        bool Create( );
        void SetupCommandSystem( ICommandSystem *cmdSystem );
        bool Frame( float timeDelta );
        void UpdateScreen( );

    private:

        IState *m_state;
        Rasterizer &m_r;
        Mesh *m_cube;
        StateVar m_vFOV;
        StateVar m_hFOV;
        StateVar m_near;
        StateVar m_far;
        StateVar m_simSpeed;
        
    };
}