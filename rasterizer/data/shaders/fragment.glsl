#version 430 core

uniform sampler2D diffuseMap;

in vec3 uv;
in vec4 normal;
out vec4 oclr;

/*vec4 Blend( vec4 src, vec4 dst )
{
	vec4 outc;
	outc.a = src.a + dst.a * ( 1 - src.a );
	if( outc.a == 0 ) {
		outc.rgb = vec3( 0.0 );
	} else {
		outc.rgb = ( src.rgb * src.a + dst.rgb * dst.a * ( 1 - src.a ) ) / outc.a;
	}
	return outc;
}*/

void main( )
{
	/*oclr = Blend( texture( textures[0], uv.xy ), oclr );
	oclr = Blend( oclr, texture( textures[1], uv.xy ) );
	oclr = Blend( oclr, texture( textures[2], uv.xy ) );*/
	oclr = texture( diffuseMap, uv.xy );
}