#version 430 core

uniform mat4 matrix;
layout( location = 0 ) in vec4 inpos;
layout( location = 1 ) in vec3 inuv;
layout( location = 2 ) in vec4 innormal;

out vec3 uv;
out vec4 normal;

void main()
{
	gl_Position = matrix * inpos;
	uv = inuv;
	normal = innormal;
}