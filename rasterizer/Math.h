#pragma once

namespace Rasterizer2
{
    class Math
    {

    public:

        static const float PI;
        static const float E;

        INLINE static float Sqrt( float x )
        {
            return sqrtf( x );
        }
        INLINE static float InvSqrt( float x )
        {
            return sqrtf( 1.f / x );
        }
        INLINE static float Sin( float a )
        {
            return sinf( a );
        }
        INLINE static float Cos( float a )
        {
            return cosf( a );
        }
        INLINE static void SinCos( float a, float &s, float &c )
        {
            // DOOM3!
    #ifdef _WIN32
	        _asm {
		        fld		a
		        fsincos
		        mov		ecx, c
		        mov		edx, s
		        fstp	dword ptr [ecx]
		        fstp	dword ptr [edx]
	        }
    #else
	        s = sinf( a );
	        c = cosf( a );
    #endif
        }
        INLINE static float Tan( float a )
        {
            return tanf( a );
        }
        INLINE static float Fabs( float a )
        {
            int tmp = *reinterpret_cast<int *>( &a );
            tmp &= 0x7FFFFFFF;
            return *reinterpret_cast<float *>( &tmp );
        }
        INLINE static float DegToRadians( float a )
        {
            return a / 180.f * PI;
        }
        INLINE static float RadToDegrees( float a )
        {
            return a / PI * 180.f;
        }

    };
}

#include "Vector.h"
#include "Matrix.h"
#include "Angles.h"