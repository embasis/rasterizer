#pragma once

#include "StateVar.h"
#include "StateCmd.h"

namespace Rasterizer2
{
    class ICommandSystem
    {

    public:

        virtual bool RegisterStateVar( StateVar *var ) = 0;
        virtual int RegisterStateVars( StateVar **vars, int varCount ) = 0;
        virtual bool RegisterStateCmd( StateCmd *cmd ) = 0;
        virtual int RegisterStateCmds( StateCmd **cmds, int cmdCount ) = 0;
        virtual StateVar *GetStateVar( const char *name ) = 0;
        virtual const StateVar *GetStateVar( const char *name ) const = 0;
        virtual bool StateVarExists( const char *name ) const = 0;
        virtual StateCmd *GetStateCmd( const char *name ) = 0;
        virtual const StateCmd *GetStateCmd( const char *name ) const = 0;
        virtual bool StateCmdExists( const char *name ) const = 0;

    };
}