#include "precompiled.h"
#include "Mesh.h"
#include "Texture.h"

namespace Rasterizer2
{
    GLuint Mesh::m_program = 0;
    GLint Mesh::m_diffuseMapSampler = 0;
    GLint Mesh::m_transfMatrixLocation = 0;
    bool Mesh::m_initialized = false;

    Mesh::Mesh( ) : m_materials( 0 ), m_groupBorders( 0 )
    {
        if( !m_initialized ) {
            // setup shaders
            GLuint vsh = LoadShader( GL_VERTEX_SHADER, "vertex.glsl" );
            GLuint psh = LoadShader( GL_FRAGMENT_SHADER, "fragment.glsl" );

            m_program = _glCreateProgram( );
            _glAttachShader( m_program, vsh );
            _glAttachShader( m_program, psh );
            _glLinkProgram( m_program );

            GLint linked;
            _glGetProgramiv( m_program, GL_LINK_STATUS, &linked );
            if( !linked ) {
                char info[DebugOutput::MAX_MESSAGE_LENGTH+1];
                _glGetProgramInfoLog( m_program, DebugOutput::MAX_MESSAGE_LENGTH+1, NULL, info );
                DebugOutput::Printf( "%s\n", info );
                DebugOutput::FatalError( "Couldn't link a shader!" );
            }

            _glDetachShader( m_program, vsh );
            _glDetachShader( m_program, psh );
            _glDeleteShader( vsh );
            _glDeleteShader( psh );

            _glUseProgram( m_program );
            m_diffuseMapSampler = _glGetUniformLocation( m_program, "diffuseMap" );
            m_transfMatrixLocation = _glGetUniformLocation( m_program, "matrix" );
            m_initialized = true;
        }

        _glGenBuffers( NUMBER_OF_BUFFERS, m_buffers );
        _glGenVertexArrays( 1, &m_vao );
        m_transform.Identity( );
    }

    Mesh::~Mesh( )
    {
        if( m_materials ) {
            delete[] m_materials;
        }
        if( m_groupBorders ) {
            delete[] m_groupBorders;
        }
        _glDeleteVertexArrays( 1, &m_vao );
        _glDeleteBuffers( NUMBER_OF_BUFFERS, m_buffers );
    }

    bool Mesh::SetVertexData( Vertex *verts, int vertCount )
    {
        DebugOutput::Printf( "Mesh::SetVertexData( ): vertex count: %d\n", vertCount );

        if( !vertCount ) {
            return false;
        }

        _glBindVertexArray( m_vao );

        _glBindBuffer( GL_ARRAY_BUFFER, m_buffers[BUFFER_VERTEX] );
        _glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex ) * vertCount, verts, GL_STATIC_DRAW );

        _glVertexAttribPointer( ATTRIB_POS, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), 0 ); 
        _glEnableVertexAttribArray( ATTRIB_POS );
        _glVertexAttribPointer( ATTRIB_UV, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const GLvoid * )sizeof( Vec3 ) );
        _glEnableVertexAttribArray( ATTRIB_UV );
        _glVertexAttribPointer( ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const GLvoid * )( 2 * sizeof( Vec3 ) ) );
        _glEnableVertexAttribArray( ATTRIB_NORMAL );

        m_vertexCount = vertCount;

        return true;
    }

    bool Mesh::SetIndexData( int *indexes, int indCount, Material *materials, int materialsCount, int *groupBorders )
    {
        DebugOutput::Printf( "Mesh::SetIndexData( ): triangle count: %d\n", indCount / 3 );

        _glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_buffers[BUFFER_INDEX] );
        _glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( int ) * indCount, indexes, GL_STATIC_DRAW );

        if( m_materials ) {
            delete[] m_materials;
        }
        if( m_groupBorders ) {
            delete[] m_groupBorders;
        }
        m_materials = new Material[materialsCount];
        m_groupBorders = new int[materialsCount];

        memcpy( m_materials, materials, sizeof( Material ) * materialsCount );
        memcpy( m_groupBorders, groupBorders, sizeof( int ) * materialsCount );
        m_materialsCount = materialsCount;
        return true;
    }

    Mat4x4 Mesh::GetTransform( ) const
    {
        return m_transform;
    }

    void Mesh::SetTransform( const Mat4x4 &transform )
    {
        m_transform = transform;
    }

    GLuint Mesh::LoadShader( GLenum type, const char *name )
    {
        if( strlen( name ) + strlen( SHADER_LOCATION ) >= MAX_PATH ) {
            DebugOutput::FatalError( "Shader name is too big!" );
        }

        GLuint shader = _glCreateShader( type );

        const int MAX_SHADER_LENGTH = 16384;
        char sh[MAX_SHADER_LENGTH+1], fullPath[MAX_PATH];
        strcpy( fullPath, SHADER_LOCATION );
        strcat( fullPath, name );

        memset( sh, 0, sizeof( sh ) );

        FILE *f = fopen( fullPath, "rb" );
        if( !f ) {
            DebugOutput::FatalError( "Failed to open needed shader file!" );
        }
        int shLen = FileLength( f );
        if( shLen > MAX_SHADER_LENGTH ) {
            DebugOutput::FatalError( "Shader file is bigger than %d bytes", MAX_SHADER_LENGTH );
        }
        fread( sh, shLen, 1, f );
        fclose( f );

        const GLchar *src = sh;
        _glShaderSource( shader, 1, &src, NULL );
        _glCompileShader( shader );

        GLint compiled;
        _glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
        if( !compiled ) {
            char info[DebugOutput::MAX_MESSAGE_LENGTH+1];
            _glGetShaderInfoLog( shader, DebugOutput::MAX_MESSAGE_LENGTH+1, NULL, info );
            DebugOutput::Printf( "%s\n", info );
            DebugOutput::FatalError( "Couldn't compile a shader!" );
        }

        return shader;
    }

    void Mesh::Draw( const View &view ) const
    {
        _glBindVertexArray( m_vao );
        _glBindBuffer( GL_ARRAY_BUFFER, m_buffers[BUFFER_VERTEX] );
        _glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_buffers[BUFFER_INDEX] );
        _glUniform1i( m_diffuseMapSampler, 0 );
        _glActiveTexture( GL_TEXTURE0 );
        
        Mat4x4 transf = view.GetMatrix( ) * m_transform;
        _glUniformMatrix4fv( m_transfMatrixLocation, 1, GL_TRUE, ( GLfloat* )&transf );

        for( int i = 0; i < m_materialsCount; i++ ) {
            _glBindTexture( GL_TEXTURE_2D, m_materials[i].GetTextureMap( Material::MAP_DIFFUSE )->GetHandle( ) );
            _glDrawElements( GL_TRIANGLES, m_groupBorders[i] - ( i > 0 ? m_groupBorders[i-1] : -1 ), GL_UNSIGNED_INT, ( const GLvoid* )( ( i > 0 ? m_groupBorders[i-1] + 1 : 0 ) * sizeof( int ) ) );
        }
    }
}
