#include "precompiled.h"
#include "Material.h"
#include "Texture.h"

namespace Rasterizer2
{
    Texture *Material::defaultTexture = 0;

    Material::Material( )
    {
    }

    Material::Material( IState *state )
    {
        if( !defaultTexture ) {
            const int WIDTH = 32, HEIGHT = 32;
            byte data[4*WIDTH*HEIGHT];
            memset( data, -1, sizeof( data ) );
            defaultTexture = state->GetTexturesManager( ).Create( );
            defaultTexture->Create( data, WIDTH, HEIGHT, PIXEL_R8G8B8A8, PIXEL_R8G8B8A8, false );
        }
        SetDefaults( );
    }

    Material::~Material( )
    {
    }

    void Material::SetDefaults( )
    {
        for( int i = 0; i < NUMBER_OF_MAPS; ++i ) {
            m_textures[i] = defaultTexture;
        }
    }
}