#pragma once

namespace Rasterizer2
{
    #ifdef near
    #undef near
    #endif
    #ifdef far
    #undef far
    #endif

    class View
    {

    public:

        INLINE View( )
        {
            m_projection.Identity( );
            m_view.Identity( );
            m_matrix.Identity( );
        }

        //                   y
        //                   |
        //                   |
        //                   |
        //                   |_ _ _ _ _ _ -z
        //                  /
        //                 /
        //                /
        //               /
        //              x
        //

        // !!!!!! near and far are z coordinates !!!!!! i.e. negative( in opengl )

        void SetOrtho( float left, float right, float bottom, float top, float near, float far );
        void SetPerspective( float left, float right, float bottom, float top, float near, float far );
        void SetPerspectiveFOV( float hfov, float vfov, float near, float far ); // fovs are in degrees
        
        INLINE void Pitch( float angle )
        {
            m_viewAngles.pitch += angle;
        }
        INLINE void Yaw( float angle )
        {
            m_viewAngles.yaw += angle;
        }
        INLINE void Roll( float angle )
        {
            m_viewAngles.roll += angle;
        }
        INLINE void Move( Vec3 v )
        {
            m_pos += v;
        }
        INLINE void SetPosition( Vec3 pos )
        {
            m_pos = pos;
        }
        INLINE void AddAngles( Angles angles )
        {
            m_viewAngles += angles;
        }
        INLINE void SetAngles( Angles angles )
        {
            m_viewAngles = angles;
        }
        INLINE Vec3 GetPosition( )
        {
            return m_pos;
        }
        INLINE Mat4x4 GetMatrix( ) const
        {
            UpdateMatrix( );
            return m_matrix;
        }
        INLINE Mat4x4 GetProjectionMatrix( ) const
        {
            UpdateMatrix( );
            return m_projection;
        }
        INLINE Mat4x4 GetViewMatrix( ) const
        {
            UpdateMatrix( );
            return m_view;
        }
        INLINE Angles GetAngles( ) const
        {
            return m_viewAngles;
        }
    
    private:

        Angles m_viewAngles;
        Vec3 m_pos;
        mutable Angles m_oldAngles;
        mutable Vec3 m_oldPos;
        mutable Mat4x4 m_projection;
        mutable Mat4x4 m_view;
        mutable Mat4x4 m_matrix;

        void UpdateMatrix( void ) const;
    };
}