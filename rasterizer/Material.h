#pragma once

#include "IState.h"

namespace Rasterizer2
{
    class Material
    {

    public:

        enum TextureMap
        {
            MAP_DIFFUSE,
            MAP_SPECULAR,
            NUMBER_OF_MAPS
        };

        Material( );
        Material( IState *state );
        ~Material( );

        INLINE void SetTextureMap( TextureMap mapType, Texture *texture )
        {
            m_textures[mapType] = texture;
        }

        INLINE Texture *GetTextureMap( TextureMap mapType ) const
        {
            return m_textures[mapType];
        }

        void SetDefaults( );

    private:

        Texture *m_textures[NUMBER_OF_MAPS];
        static Texture *defaultTexture;

    };
}