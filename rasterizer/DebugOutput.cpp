// DebugOutput.cpp - just a quake3 copypaste + modifications

#include "precompiled.h"
#include "DebugOutput.h"

namespace {
    HWND g_hInputLine, g_hBuffer, g_hSubmitButton, g_hCopyButton, g_hQuitButton, g_hConWnd, g_hTopBuf;
    WNDPROC g_wndprocInputLine;
    FILE *g_log;
    bool g_initialized = false;
    int WINAPI InputLineProc( HWND hWnd, UINT uMsg, WPARAM wprm, LPARAM lprm );
    int WINAPI ConsoleWindowProc( HWND hWnd, UINT uMsg, WPARAM wprm, LPARAM lprm );
}

enum
{
    INPUTID = 50,
    BUFFERID,
    SUBMITID,
    COPYID,
    QUITID,
    TOPBUFID
};

namespace Rasterizer2
{
    void DebugOutput::CreateConsole( HINSTANCE hInst )
    {
        WNDCLASS wc;
        const char *classname = "rasterizerDebugOutput";
        int style = WS_POPUPWINDOW | WS_CAPTION | WS_MINIMIZEBOX;

        memset( &wc, 0, sizeof( wc ) );

        wc.hCursor       = LoadCursor( NULL, IDC_ARROW );
        wc.hIcon		  = LoadIcon( NULL, IDC_ICON );
        wc.hbrBackground = ( HBRUSH )COLOR_WINDOW;
        wc.hInstance     = hInst;
        wc.lpszClassName = classname;
        wc.lpfnWndProc   = ( WNDPROC )::ConsoleWindowProc;

        RegisterClass( &wc );
	
        RECT r;

        r.bottom = 450;
        r.left   = 0;
        r.right  = 540;
        r.top    = 0;

        AdjustWindowRect( &r, style, FALSE );

        HDC hdc = GetDC( GetDesktopWindow( ) );
        int swidth = GetDeviceCaps( hdc, HORZRES );
        int sheight = GetDeviceCaps( hdc, VERTRES );
        ReleaseDC( GetDesktopWindow( ), hdc );
	
        g_hConWnd = CreateWindowEx( 0, classname, "Debug", style, ( swidth - 600 ) / 2, ( sheight - 450 ) / 2 , r.right - r.left + 1, r.bottom - r.top + 1,
                                    NULL, NULL, hInst, NULL );
	
        hdc = GetDC( g_hConWnd );
        int fheight = -MulDiv( 8, GetDeviceCaps( hdc, LOGPIXELSY ), 72 );
        ReleaseDC( g_hConWnd, hdc );

        HFONT f       = CreateFont( fheight, 0, 0, 0, FW_LIGHT, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_MODERN | FIXED_PITCH, "Courier New" );
        g_hInputLine    = CreateWindow( "edit", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_AUTOHSCROLL, 6, 400, 528, 20, g_hConWnd, ( HMENU )INPUTID, hInst, NULL );
        g_hSubmitButton = CreateWindow( "button", NULL, BS_PUSHBUTTON | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 5, 425, 72, 24, g_hConWnd, ( HMENU )SUBMITID, hInst, NULL );
        g_hCopyButton   = CreateWindow( "button", NULL, BS_PUSHBUTTON | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 82, 425, 72, 24, g_hConWnd, ( HMENU )COPYID, hInst, NULL );
        g_hQuitButton   = CreateWindow( "button", NULL, BS_PUSHBUTTON | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 462, 425, 72, 24, g_hConWnd, ( HMENU )QUITID, hInst, NULL );
        g_hBuffer       = CreateWindow( "edit", NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_BORDER | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | ES_READONLY, 6, 40, 526, 354,
                                        g_hConWnd, ( HMENU )BUFFERID, hInst, NULL );
        g_hTopBuf = CreateWindow( "edit", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_MULTILINE | ES_READONLY, 6, 3, 526, 34, g_hConWnd, ( HMENU )TOPBUFID, hInst, NULL );

        SendMessage( g_hSubmitButton, WM_SETTEXT, NULL, ( LPARAM )"Submit" );
        SendMessage( g_hCopyButton, WM_SETTEXT, NULL, ( LPARAM )"Copy" );
        SendMessage( g_hQuitButton, WM_SETTEXT, NULL, ( LPARAM )"Quit" );
        SendMessage( g_hBuffer, WM_SETFONT, ( WPARAM )f, NULL );
        SendMessage( g_hInputLine, WM_SETFONT, ( WPARAM )f, NULL );
        SendMessage( g_hTopBuf, WM_SETFONT, ( WPARAM )f, NULL );

        g_wndprocInputLine = (WNDPROC)SetWindowLong( g_hInputLine, GWL_WNDPROC, ( int )InputLineProc );

        ShowWindow( g_hConWnd, SW_HIDE );
        UpdateWindow( g_hConWnd );
        SetForegroundWindow( g_hConWnd );
        SetFocus( g_hInputLine );

        g_log = fopen( "log.txt", "w+" );

        g_initialized = true;
    }

    void DebugOutput::Printf( const char *text, ... )
    {
        if( !g_initialized )
            return;

        char msg[MAX_MESSAGE_LENGTH+1];
        char buffer[MAX_MESSAGE_LENGTH*2+1];
        char *b = buffer;
        const char *msga;
        int bufLen, i = 0;
        static uint s_totalChars;
        va_list args;

        va_start( args, text );
        vsprintf( msg, text, args );
        va_end( args );

        if ( strlen( msg ) > MAX_MESSAGE_LENGTH - 1 )
        {
            msga = msg + strlen( msg ) - MAX_MESSAGE_LENGTH + 1;
        }
        else
        {
            msga = msg;
        }
        while ( msga[i] && ( ( b - buffer ) < sizeof( buffer ) - 1 ) )
        {
            if ( msga[i] == '\n' && msga[i+1] == '\r' )
            {
                b[0] = '\r';
                b[1] = '\n';
                b += 2;
                i++;
            }
            else if ( msga[i] == '\r' )
            {
                b[0] = '\r';
                b[1] = '\n';
                b += 2;
            }
            else if ( msga[i] == '\n' )
            {
                b[0] = '\r';
                b[1] = '\n';
                b += 2;
            }
            else
            {
                *b= msga[i];
                b++;
            }
            i++;
        }
        *b = 0;
        bufLen = b - buffer;

        s_totalChars += bufLen;
        if ( s_totalChars > 0x7fff )
        {
            SendMessage( g_hBuffer, EM_SETSEL, 0, -1 );
            s_totalChars = bufLen;
        }

        SendMessage( g_hBuffer, EM_LINESCROLL, 0, 0xffff );
        SendMessage( g_hBuffer, EM_SCROLLCARET, 0, 0 );
        SendMessage( g_hBuffer, EM_REPLACESEL, 0, (LPARAM) buffer );

        //fwrite( buffer + 1, bufLen - 1, 1, g_log );
        fprintf( g_log, "%s", buffer );
    }

    void DebugOutput::ShowConsole( int showCmd )
    {
        ShowWindow( g_hConWnd, showCmd );
    }

    void DebugOutput::FatalError( const char *text, ... )
    {
        char msg[MAX_MESSAGE_LENGTH+1];
        va_list args;

        va_start( args, text );
        vsprintf( msg, text, args );
        va_end( args );

        MessageBox( NULL, text, "FATAL ERROR", MB_ICONERROR | MB_OK );

        exit( 0 );
    }

    void DebugOutput::SetTopbufferText( const char *text, ... )
    {
        char msg[MAX_MESSAGE_LENGTH+1];
        va_list args;

        va_start( args, text );
        vsprintf( msg, text, args );
        va_end( args );

        SendMessage( g_hTopBuf, WM_SETTEXT, NULL, ( LPARAM )msg );
    }

    void DebugOutput::MoveToCorner( void )
    {
        RECT r, workArea;
        r.bottom = 450;
        r.left   = 0;
        r.right  = 540;
        r.top    = 0;
        AdjustWindowRect( &r, WS_POPUPWINDOW | WS_CAPTION | WS_MINIMIZEBOX, FALSE );

        HDC hdc = GetDC( GetDesktopWindow( ) );
        int swidth = GetDeviceCaps( hdc, HORZRES ), sheight = GetDeviceCaps( hdc, VERTRES );
        ReleaseDC( GetDesktopWindow( ), hdc );

        // determine Windows Taskbar height
        SystemParametersInfo( SPI_GETWORKAREA, 0, &workArea, 0 );
        int taskbarHeight = sheight - workArea.bottom;

        int width = r.right - r.left + 1, height = r.bottom - r.top + 1;

        MoveWindow( g_hConWnd, swidth - width, sheight - height - taskbarHeight, width, height, FALSE );
    }
}

namespace {

    using Rasterizer2::DebugOutput;

    int WINAPI InputLineProc( HWND hWnd, UINT uMsg, WPARAM wprm, LPARAM lprm )
    {
        char buffer[DebugOutput::MAX_MESSAGE_LENGTH+1];

        switch( uMsg )
        {
        case WM_KILLFOCUS:
            if( (HWND)wprm == g_hConWnd )
            {
                SetFocus( hWnd );
                return 0;
            }
            break;
        case WM_CHAR:
            if( wprm == 13 )
            {
                SendMessage( g_hInputLine, WM_GETTEXT, DebugOutput::MAX_MESSAGE_LENGTH, ( LPARAM )buffer );
                SetWindowText( g_hInputLine, "" );

                DebugOutput::Printf( ">%s\n", buffer );
                strcat( buffer, ";" );
                //ngCommandSystem::GetInstance( )->BufferString( buffer );
            }
            break;
        }

        return CallWindowProc( g_wndprocInputLine, hWnd, uMsg, wprm, lprm );
    }

    int WINAPI ConsoleWindowProc( HWND hWnd, UINT uMsg, WPARAM wprm, LPARAM lprm )
    {
        switch( uMsg )
        {
        case WM_ACTIVATE:
            if( LOWORD( wprm ) != WA_INACTIVE )
                SetFocus( g_hInputLine );
            break;
        case WM_CLOSE:
            exit( 0 );
            break;
        case WM_CREATE:
            SetTimer( hWnd, 1, 1000, NULL );
            break;
        case WM_COMMAND:
            if( wprm == COPYID )
            {
                SendMessage( g_hBuffer, EM_SETSEL, 0, -1 );
                SendMessage( g_hBuffer, WM_COPY, 0, 0 );
            }
            else if( wprm == QUITID )
                exit( 0 );
            else if( wprm == SUBMITID )
                SendMessage( g_hInputLine, WM_CHAR, 13, 0 );
            break;
        }

        return DefWindowProc( hWnd, uMsg, wprm, lprm );
    }
}
