#pragma once

#include "Material.h"
#include "SceneObject.h"

namespace Rasterizer2
{
#pragma pack(push, 1)
    struct Vertex
    {
        Vec3 pos;
        Vec3 uv;
        Vec3 normal;
    };
#pragma pack(pop)

	class Mesh : public SceneObject
	{

    public:

        Mesh( );
        ~Mesh( );

		bool SetVertexData( Vertex *verts, int vertCount );
        bool SetIndexData( int *indexes, int indCount, Material *materials, int materialsCount, int *groupBorders );
        Mat4x4 GetTransform( ) const;
        void SetTransform( const Mat4x4 &transform );
        void Draw( const View &view ) const;

	private:

        enum
        {
            BUFFER_VERTEX,
            BUFFER_INDEX,
            NUMBER_OF_BUFFERS
        };

        enum
        {
            ATTRIB_POS,
            ATTRIB_UV,
            ATTRIB_NORMAL
        };

		Mat4x4 m_transform;
		Vec3 m_boundingSphere;
		GLuint m_vao;
		GLuint m_buffers[2];
        Material *m_materials;
        int m_materialsCount;
        int m_vertexCount;
        int *m_groupBorders;

        static GLuint m_program;
        static GLint m_diffuseMapSampler;
        static GLint m_transfMatrixLocation;
        static bool m_initialized; // are shaders initialized?

        GLuint LoadShader( GLenum type, const char *name );

	};
}