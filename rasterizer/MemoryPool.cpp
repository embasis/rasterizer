#include "precompiled.h"

namespace Rasterizer2
{
    MemoryPool::MemoryPool( ) : m_memory( nullptr ), m_poolSize( 0 ), m_allocatedSize( 0 ), m_allocatedBlocksCount( 0 ), m_blocksHead( nullptr ),
        m_blocksTail( nullptr ), m_additionalMemoryHead( nullptr ), m_additionalMemoryBlocksCount( 0 ), m_additionalMemorySize( 0 )
    {
    }

    MemoryPool::~MemoryPool( )
    {
        if( m_memory ) {
            delete[] m_memory;
        }
        while( m_additionalMemoryHead ) {
            Block *tmp = m_additionalMemoryHead->next;
            delete[]( byte* )m_additionalMemoryHead;
            m_additionalMemoryHead = tmp;
        }
    }

    bool MemoryPool::Initialize( uint poolSize )
    {
        if( poolSize > MAX_POOL_SIZE ) {
            DebugOutput::FatalError( "ATTEMPTED TO CREATE MEMORY POOL WITH SIZE > %u bytes!", MAX_POOL_SIZE );
            return false;
        }

        m_memory = new byte[poolSize];
        Block *block = ( Block* )m_memory;
        block->length = 0;
        block->next = nullptr;
        block->prev = nullptr;
        m_blocksHead = block;
        m_blocksTail = block;
        m_poolSize = poolSize;

        DebugOutput::Printf( "Memory pool was initialized with %u bytes of memory\n", poolSize );
        
        return true;
    }

    byte *MemoryPool::Allocate( uint size )
    {
        const uint bytesNeed = size + sizeof( Block );
        if( bytesNeed > m_poolSize - m_allocatedSize - GetOverheadSize( ) ) {
            DebugOutput::Printf( "MemoryPool::Allocate: current pool's free memory size %u is not enough to allocate memory of size %u\n",
                m_poolSize - m_allocatedSize, size );
            return AllocateAdditionalMemoryBytes( size );
        }
        
        for( Block *cur = m_blocksTail; cur; cur = cur->prev ) {
            Block *next = cur->next, *start = ( Block* )( ( byte* )cur + sizeof( Block ) + cur->length );
            if( ( next ? next - start : m_poolSize - ( ( byte* )start - m_memory ) ) >= bytesNeed ) {
                start->length = size;
                start->next = next;
                start->prev = cur;
                if( next ) {
                    next->prev = start;
                } else {
                    m_blocksTail = start;
                }
                cur->next = start;
                ++m_allocatedBlocksCount;
                m_allocatedSize += size;
                return ( byte* )( start + 1 );
            }
        }

        DebugOutput::Printf( "MemoryPool::Allocate: couldn't fit memory block of size %u in the pool's free memory space\n", size );

        return AllocateAdditionalMemoryBytes( size );
    }

    void MemoryPool::Free( byte *ptr )
    {
        if( ptr < m_memory || ptr >= m_memory + m_poolSize ) {
            FreeAdditionalMemoryBytes( ptr );
        }

        Block *block = ( Block* )( ptr - sizeof( Block ) );
        block->prev->next = block->next; // block->prev != nullptr, only dummy block has block->prev == nullptr
        if( block->next ) {
            block->next->prev = block->prev;
        }
        if( block == m_blocksTail ) {
            m_blocksTail = block->prev;
        }

        m_allocatedSize -= block->length;
        --m_allocatedBlocksCount;
    }

    void MemoryPool::FreeAll( )
    {
        m_blocksHead->next = nullptr;
        m_blocksTail = m_blocksHead;
        m_allocatedSize = 0;
        m_allocatedBlocksCount = 0;
    }

    byte *MemoryPool::AllocateAdditionalMemoryBytes( uint size )
    {
        byte *memory = new byte[size + sizeof( Block )];
        Block *block = ( Block* )memory;
        block->length = size;
        block->next = m_additionalMemoryHead;
        block->prev = nullptr;
        if( m_additionalMemoryHead ) {
            m_additionalMemoryHead->prev = block;
        }
        m_additionalMemoryHead = block;
        ++m_additionalMemoryBlocksCount;
        m_additionalMemorySize += size;
        return ( byte* )( block + 1 );
    }

    void MemoryPool::FreeAdditionalMemoryBytes( byte *ptr )
    {
        Block *block = ( Block* )( ptr - sizeof( Block ) );
        if( block->prev ) {
            block->prev->next = block->next;
        }
        if( block->next ) {
            block->next->prev = block->prev;
        }
        m_additionalMemorySize -= block->length;
        --m_additionalMemoryBlocksCount;
        delete[]( byte* )block;
    }
}