#include "precompiled.h"
#include "File.h"

namespace Rasterizer2
{
    File::File( )
    {
        *m_path = 0;
    }
    
    File::~File( )
    {
    }

    bool File::LoadFile( const char *path )
    {
        DebugOutput::Printf( "loading file \"%s\"\n", path );
        if( strlen( path ) + strlen( GetFileLocation( ) ) >= MAX_PATH ) {
            DebugOutput::Printf( "\"%s\": file name is too long!\n", path );
            return false;
        }

        char fullPath[MAX_PATH];
        strcpy( fullPath, GetFileLocation( ) );
        strcat( fullPath, path );

        FILE *f = fopen( fullPath, "r" );
        if( !f ) {
            DebugOutput::Printf( "\"%s\": failed to open\n", path );
            return false;
        }

        if( !Parse( f ) ) {
            DebugOutput::Printf( "\"%s\": parsing failed\n", path );

            Clear( );
            fclose( f );

            return false;
        } else {
            DebugOutput::Printf( "\"%s\" was loaded successfully\n", path );
        }

        fclose( f );

        strcpy( m_path, path );
        strcpy( m_fileName, m_path );
        StripFileExtension( m_fileName );

        return true;
    }

    bool File::LoadFileFromMemory( const char *name, const void *buffer )
    {
        DebugOutput::Printf( "loading file \"%s\" from memory\n", name );
        if( strlen( name ) >= MAX_PATH ) {
            DebugOutput::Printf( "\"%s\": file name is too long!\n", name );
            return false;
        }

        if( !ParseFromMemory( buffer ) ) {
            DebugOutput::Printf( "\"%s\": parsing failed\n", name );
            Clear( );
            return false;
        } else {
            DebugOutput::Printf( "\"%s\" was loaded successfully\n", name );
        }

        strcpy( m_path, name );
        strcpy( m_fileName, m_path );
        StripFileExtension( m_fileName );

        return true;
    }
}