#pragma once

namespace Rasterizer2
{
    class MemoryPool
    {

    public:

        static const uint MAX_POOL_SIZE = 1U << 29;

        MemoryPool( );
        ~MemoryPool( );

        bool Initialize( uint poolSize );
        byte *Allocate( uint size );
        void Free( byte *ptr );
        void FreeAll( );

        template< typename T >
        INLINE T *NewObjects( uint count )
        {
            return new( Allocate( count * sizeof( T ) ) ) T[count];
        }
        
        template< typename T >
        INLINE T *DeleteObjects( T *ptr )
        {
            ptr->~T( );
            Free( ( byte* )ptr );
        }

        INLINE static const uint GetBlockSize( )
        {
            return sizeof( Block );
        }

        INLINE uint GetSize( ) const
        {
            return m_poolSize;
        }

        INLINE uint GetAdditionalMemorySize( ) const
        {
            return m_additionalMemorySize;
        }

        INLINE uint GetOverheadSize( ) const
        {
            return /*sizeof( Rasterizer2::MemoryPool ) + */sizeof( Block ) * ( m_allocatedBlocksCount + 1 ); // +1 is because of the dummy block at the very start of m_memory
        }

        // Includes pool size + additional memory size + complete overhead
        INLINE uint GetTotalSize( ) const
        {
            return GetSize( ) + GetAdditionalMemorySize( ) + GetOverheadSize( );
        }
        
    private:

        byte *AllocateAdditionalMemoryBytes( uint size );
        void FreeAdditionalMemoryBytes( byte *ptr );

        struct Block
        {
            uint length;
            Block *prev;
            Block *next;
        };

        byte *m_memory;
        uint m_poolSize;
        uint m_allocatedSize;
        uint m_allocatedBlocksCount;
        Block *m_blocksHead;
        Block *m_blocksTail;
        Block *m_additionalMemoryHead;
        uint m_additionalMemoryBlocksCount;
        uint m_additionalMemorySize;

    };

    template< typename T, int poolSize >
    class MemoryPoolAllocator
    {

    public:

        typedef T value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef std::size_t size_type;
        typedef std::ptrdiff_t difference_type;

        template< typename U >
        struct rebind
        {
            typedef MemoryPoolAllocator< U, poolSize > other;
        };

        INLINE MemoryPoolAllocator( ) : m_pool( new MemoryPool( ) )
        {
            m_pool->Initialize( poolSize );
        }

        INLINE ~MemoryPoolAllocator( )
        {
        }

        template< typename U >
        INLINE MemoryPoolAllocator( const MemoryPoolAllocator< U, poolSize >& a ) : m_pool( a.m_pool )
        {
        }

        INLINE pointer address( reference r )
        {
            return &r;
        }

        INLINE const_pointer address( const_reference r )
        {
            return &r;
        }

        INLINE pointer allocate( size_type cnt, typename std::allocator< void >::const_pointer = nullptr )
        {
            return ( pointer )m_pool->Allocate( cnt * sizeof( T ) );
        }

        INLINE void deallocate( pointer p, size_type )
        {
            m_pool->Free( ( byte* )p );
        }

        INLINE size_type max_size( ) const
        {
            return poolSize / sizeof( T );
        }

        INLINE void construct( pointer p, const T& t )
        {
            new( p ) T( t );
        }

        INLINE void destroy( pointer p )
        {
            p->~T( );
        }

        INLINE bool operator==( const MemoryPoolAllocator & )
        {
            return true;
        }

        INLINE bool operator!=( const MemoryPoolAllocator & a )
        {
            return !operator==( a );
        }

        std::shared_ptr< MemoryPool > m_pool;

    };
}