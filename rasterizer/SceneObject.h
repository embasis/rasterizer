#pragma once

#include "View.h"

namespace Rasterizer2
{
    class SceneObject
    {

    public:

        virtual INLINE Mat4x4 GetTransform( ) const
        {
            return m_transform;
        }
        virtual INLINE void SetTransform( const Mat4x4 &transform )
        {
            m_transform = transform;
        }
        virtual void Draw( const View &view ) const = 0;

    private:

        Mat4x4 m_transform;

    };
}