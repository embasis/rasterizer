#pragma once

namespace Rasterizer2
{
    class DebugOutput
    {

    public:

        static const int MAX_MESSAGE_LENGTH = 16834;

#ifdef _WIN32
        static void CreateConsole( HINSTANCE hInst );
#else
        static void CreateInputThread( );
#endif
        static void ShowConsole( int showCmd );
        static void Printf( const char *text, ... );
        static void FatalError( const char *text, ... );
        static void SetTopbufferText( const char *text, ... );
        static void MoveToCorner( void );

    };
}
