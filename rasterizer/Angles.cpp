#include "precompiled.h"

namespace Rasterizer2
{
    void Angles::ToVectors( Mat3x3 &vec, bool forward, bool right, bool up ) const
    {
	    float a, b, c, d, e, f;
	    Math::SinCos( pitch, b, a );
	    Math::SinCos( yaw, d, c );
	    Math::SinCos( roll, f, e );
	    if( forward ) {
		    vec[0] = Vec3( -a*d*e - b*f, b*e - a*d*f, -a*c );
	    }
	    if( right ) {
		    vec[1] = Vec3( c*e, c*f, -d );
	    }
	    if( up ) {
		    vec[2] = Vec3( b*d*e - a*f, a*e + b*d*f, b*c );
	    }
    }
}