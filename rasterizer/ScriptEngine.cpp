#include "precompiled.h"
#include "ScriptEngine.h"

namespace Rasterizer2
{

    ScriptEngine::ScriptEngine( ) : m_stringPool( ), m_slots( m_stringPool ), m_rootScript( nullptr ), m_curScript( nullptr )
    {
        m_stringPool.Initialize( STRING_POOL_SIZE );

        DebugOutput::Printf( "ScriptEngine string pool was initialized\n" );
    }

    void ScriptEngine::Execute( Script *script, const char **argv, int argc )
    {
        m_stringPool.FreeAll( );
    }
}