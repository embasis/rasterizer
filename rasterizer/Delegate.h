#pragma once

namespace Rasterizer2
{
    template< typename ReturnType, typename ... FunctionArgs >
    class Invoker
    {

    public:

        virtual ~Invoker( )
        {
        }

        virtual ReturnType Invoke( FunctionArgs ... ) const = 0;

    };

    template< class T, typename ReturnType, typename ... FunctionArgs >
    class MemberFunction : public Invoker< ReturnType, FunctionArgs ... >
    {

    public:

        typedef ReturnType ( T::*FunctionPointer ) ( FunctionArgs ... );
        typedef ReturnType ( T::*ConstFunctionPointer ) ( FunctionArgs ... ) const;

        INLINE MemberFunction( T *instance, FunctionPointer func ) : MemberFunction( instance, func, nullptr )
        {
        }

        INLINE MemberFunction( T *instance, ConstFunctionPointer func ) : MemberFunction( instance, nullptr, func )
        {
        }

        INLINE ~MemberFunction( )
        {
        }

        INLINE ReturnType Invoke( FunctionArgs ... args ) const
        {
            if( m_func ) { 
                return ( m_instance->*m_func )( args ... );
            }
            return ( m_instance->*m_funcConst )( args ... );
        }

    private:

        INLINE MemberFunction( T *instance, FunctionPointer func, ConstFunctionPointer funcConst ) : 
            m_instance( instance ), m_func( func ), m_funcConst( funcConst )
        {
        }

        T *m_instance;
        FunctionPointer m_func;
        ConstFunctionPointer m_funcConst;

    };

    template< typename ReturnType, typename ... FunctionArgs >
    class Delegate
    {

    public:

        typedef ReturnType ( *FunctionPointer ) ( FunctionArgs ... );

        template< class T >
        INLINE Delegate( T *instance, typename MemberFunction< T, ReturnType, FunctionArgs ... >::FunctionPointer func ) : 
            m_invoker( new MemberFunction< T, ReturnType, FunctionArgs ... >( instance, func ) ), m_func( nullptr )
        {
        }

        template< class T >
        INLINE Delegate( T *instance, typename MemberFunction< T, ReturnType, FunctionArgs ... >::ConstFunctionPointer func ) :
            m_invoker( new MemberFunction< T, ReturnType, FunctionArgs ... >( instance, func ) ), m_func( nullptr )
        {
        }

        INLINE Delegate( FunctionPointer func ) : m_invoker( nullptr ), m_func( func )
        {
        }

        INLINE Delegate( const Delegate< ReturnType, FunctionArgs ... > &d ) : m_invoker( d.m_invoker ), m_func( d.m_func )
        {
        }

        INLINE ~Delegate( )
        {
        }

        INLINE ReturnType operator()( FunctionArgs ... args ) const
        {
            if( m_invoker ) {
                return m_invoker->Invoke( args ... );
            }
            return m_func( args ... );
        }

        INLINE Delegate< ReturnType, FunctionArgs ... >& operator=( const Delegate< ReturnType, FunctionArgs ... > &d )
        {
            m_invoker = d.m_invoker;
            m_func = d.m_func;
            return *this;
        }

    private:

        std::shared_ptr< Invoker< ReturnType, FunctionArgs ... > > m_invoker;
        FunctionPointer m_func;

    };
}