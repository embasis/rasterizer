#pragma once

#include "Resource.h"
#include "Script.h"

namespace Rasterizer2
{
    class Texture;
    class Mesh;

    class IState
    {

    public:

        virtual ResourceHolder< Texture > &GetTexturesManager( ) = 0;
        virtual const ResourceHolder< Texture > &GetTexturesManager( ) const = 0;
        virtual ResourceHolder< Mesh > &GetMeshesManager( ) = 0;
        virtual const ResourceHolder< Mesh > &GetMeshesManager( ) const = 0;
        virtual ResourceHolder< Script > &GetScriptsManager( ) = 0;
        virtual const ResourceHolder< Script > &GetScriptsManager( ) const = 0;

    };
}