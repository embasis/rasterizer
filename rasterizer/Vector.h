#pragma once

namespace Rasterizer2
{
    const float VEC_EPSILON = 1e-6f;

    class Vec2
    {

    public:

        float x, y;

        INLINE Vec2( )
        {
            Zero( );
        }
        // set vector components
        INLINE Vec2( const float xx, const float xy )
        {
            x = xx;
            y = xy;
        }
        INLINE float& operator[]( int index )
        {
            return ( &x )[index];
        }
        INLINE float operator[]( int index ) const
        {
            return ( &x )[index];
        }
        INLINE Vec2 operator+( const Vec2 &v ) const
        {
            return Vec2( x + v.x, y + v.y );
        }
        INLINE Vec2 operator+( float a ) const
        {
            return Vec2( x + a, y + a );
        }
        INLINE Vec2 operator-( const Vec2 &v ) const
        {
            return Vec2( x - v.x, y - v.y );
        }
        INLINE Vec2 operator-( float a ) const
        {
            return Vec2( x - a, y - a );
        }
        INLINE Vec2 operator-( ) const
        {
            return Vec2( -x, -y );
        }
        INLINE Vec2& operator+=( const Vec2 &v )
        {
            x += v.x;
            y += v.y;
            return *this;
        }
        INLINE Vec2& operator+=( float a )
        {
            x += a;
            y += a;
            return *this;
        }
        INLINE Vec2& operator-=( const Vec2 &v )
        {
            x -= v.x;
            y -= v.y;
            return *this;
        }
        INLINE Vec2& operator-=( float a )
        {
            x -= a;
            y -= a;
            return *this;
        }
        INLINE Vec2 operator*( float a ) const
        {
            return Vec2( x*a, y*a );
        }
        INLINE Vec2& operator*=( float a )
        {
            x *= a;
            y *= a;
            return *this;
        }
        INLINE bool operator==( const Vec2 &v ) const
        {
            return x == v.x && y == v.y;
        }
        INLINE float DotProduct( Vec2 v ) const
        {
            return x*v.x + y*v.y;
        }
        INLINE float WedgeProduct( Vec2 v ) const
        {
            return x*v.y - y*v.x;
        }
        INLINE float Length( ) const
        {
            return Math::Sqrt( x*x + y*y );
        }
        INLINE float SquaredLength( ) const
        {
            return x*x + y*y;
        }
        INLINE void Normalize( )
        {
            float ilength = 1.f / Length( );
            x *= ilength;
            y *= ilength;
        }
        // get data
        INLINE void GetData( float out[2] ) const
        {
            out[0] = x;
            out[1] = y;
        }
        INLINE void Zero( )
        {
            x = 0.f;
            y = 0.f;
        }

    };

    class Vec3
    {

    public:

        float x, y, z;

        INLINE Vec3( )
        {
            Zero( );
        }
        
        INLINE Vec3( Vec2 v )
        {
            x = v.x;
            y = v.y;
            z = 0.f;
        }
        // set vector components
        INLINE Vec3( const float xx, const float xy, const float xz )
        {
            x = xx;
            y = xy;
            z = xz;
        }
        INLINE float& operator[]( int index )
        {
            return ( &x )[index];
        }
        INLINE float operator[]( int index ) const
        {
            return ( &x )[index];
        }
        INLINE Vec3 operator+( const Vec3 &v ) const
        {
            return Vec3( x + v.x, y + v.y, z + v.z );
        }
        INLINE Vec3 operator+( float a ) const
        {
            return Vec3( x + a, y + a, z + a );
        }
        INLINE Vec3 operator-( const Vec3 &v ) const
        {
            return Vec3( x - v.x, y - v.y, z - v.z );
        }
        INLINE Vec3 operator-( float a ) const
        {
            return Vec3( x - a, y - a, z - a );
        }
        INLINE Vec3 operator-( void ) const
        {
            return Vec3( -x, -y, -z );
        }
        INLINE Vec3& operator+=( const Vec3 &v )
        {
            x += v.x;
            y += v.y;
            z += v.z;
            return *this;
        }
        INLINE Vec3& operator+=( float a )
        {
            x += a;
            y += a;
            z += a;
            return *this;
        }
        INLINE Vec3& operator-=( const Vec3 &v )
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
            return *this;
        }
        INLINE Vec3& operator-=( float a )
        {
            x -= a;
            y -= a;
            z -= a;
            return *this;
        }
        INLINE Vec3 operator*( float a ) const
        {
            return Vec3( x*a, y*a, z*a );
        }
        INLINE Vec3& operator*=( float a )
        {
            x *= a;
            y *= a;
            z *= a;
            return *this;
        }
        INLINE bool operator==( const Vec3 &v ) const
        {
            return x == v.x && y == v.y && z == v.z;
        }
        INLINE bool operator!=( const Vec3 &v ) const
        {
            return !( *this == v );
        }
        INLINE float DotProduct( Vec3 v ) const
        {
            return x*v.x + y*v.y + z*v.z;
        }
        INLINE Vec3 CrossProduct( Vec3 v ) const
        {
            return Vec3( y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x );
        }
        INLINE float Length( ) const
        {
            return Math::Sqrt( x*x + y*y + z*z );
        }
        INLINE float SquaredLength( ) const
        {
            return x*x + y*y + z*z;
        }
        INLINE void Normalize( )
        {
            float ilength = 1.f / Length( );
            x *= ilength;
            y *= ilength;
            z *= ilength;
        }
        // get data
        INLINE void GetData( float out[3] ) const
        {
            out[0] = x;
            out[1] = y;
            out[2] = z;
        }
        INLINE void Zero( )
        {
            x = 0.f;
            y = 0.f;
            z = 0.f;
        }

    };

    class Vec4
    {

    public:

        float x, y, z, w;

        INLINE Vec4( )
        {
            Zero( );
            w = 1.f;
        }
        INLINE Vec4( Vec3 v )
        {
            x = v.x;
            y = v.y;
            z = v.z;
            w = 1.f;
        }
        // set vector components
        INLINE Vec4( const float xx, const float xy, const float xz, const float xw )
        {
            x = xx;
            y = xy;
            z = xz;
            w = xw;
        }
        INLINE float& operator[]( int index )
        {
            return ( &x )[index];
        }
        INLINE float operator[]( int index ) const
        {
            return ( &x )[index];
        }
        INLINE Vec4 operator+( const Vec4 &v ) const
        {
            return Vec4( x + v.x, y + v.y, z + v.z, w + v.w );
        }
        INLINE Vec4 operator+( float a ) const
        {
            return Vec4( x + a, y + a, z + a, w + a );
        }
        INLINE Vec4 operator-( const Vec4 &v ) const
        {
            return Vec4( x - v.x, y - v.y, z - v.z, w - v.w );
        }
        INLINE Vec4 operator-( float a ) const
        {
            return Vec4( x - a, y - a, z - a, w - a );
        }
        INLINE Vec4 operator-( ) const
        {
            return Vec4( -x, -y, -z, -w );
        }
        INLINE Vec4& operator+=( const Vec4 &v )
        {
            x += v.x;
            y += v.y;
            z += v.z;
            w += v.w;
            return *this;
        }
        INLINE Vec4& operator+=( float a )
        {
            x += a;
            y += a;
            z += a;
            w += a;
            return *this;
        }
        INLINE Vec4& operator-=( const Vec4 &v )
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
            w -= v.w;
            return *this;
        }
        INLINE Vec4& operator-=( float a )
        {
            x -= a;
            y -= a;
            z -= a;
            w -= a;
            return *this;
        }
        INLINE Vec4 operator*( float a ) const
        {
            return Vec4( x*a, y*a, z*a, w*a );
        }
        INLINE Vec4& operator*=( float a )
        {
            x *= a;
            y *= a;
            z *= a;
            w *= a;
            return *this;
        }
        INLINE bool operator==( const Vec4 &v ) const
        {
            return x == v.x && y == v.y && z == v.z && w == v.w;
        }
        INLINE float DotProduct( Vec4 v ) const
        {
            return x*v.x + y*v.y + z*v.z + w*v.w;
        }
        INLINE float Length( ) const
        {
            return Math::Sqrt( x*x + y*y + z*z + w*w );
        }
        INLINE float SquaredLength( ) const
        {
            return x*x + y*y + z*z + w*w;
        }
        INLINE void PerspectiveDivision( )
        {
            if( Math::Fabs( w ) > VEC_EPSILON ) {
                float iw = 1.f / w;
                x *= iw;
                y *= iw;
                z *= iw;
                w = 1.f;
            }
        }
        INLINE void Normalize( )
        {
            float ilength = 1.f / Length( );
            x *= ilength;
            y *= ilength;
            z *= ilength;
            w *= ilength;
        }
        // get data
        INLINE void GetData( float out[4] ) const
        {
            out[0] = x;
            out[1] = y;
            out[2] = z;
            out[3] = w;
        }
        INLINE void Zero( )
        {
            x = 0.f;
            y = 0.f;
            z = 0.f;
            w = 0.f;
        }

    };
}