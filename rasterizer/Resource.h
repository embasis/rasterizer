#pragma once

namespace Rasterizer2
{
    const int RESOURCEHOLDER_DEFAULT_SIZE = 1024;

    template< typename T >
    class ResourceHolder
    {

    public:

        ResourceHolder( int size = RESOURCEHOLDER_DEFAULT_SIZE ) : m_size( size ), m_holder( new T*[size] )
        {
            memset( m_holder, 0, sizeof( T* ) * m_size );
        }

        ~ResourceHolder( )
        {
            for( int i = 0; i < m_size; ++i ) {
                if( m_holder[i] ) {
                    delete m_holder[i];
                }
            }
            delete[] m_holder;
        }

        T *Create( )
        {
            for( int i = 0; i < m_size; ++i ) {
                if( !m_holder[i] ) {
                    m_holder[i] = new T;
                    return m_holder[i];
                }
            }
            return 0;
        }

        bool Destroy( T *res )
        {
            for( int i = 0; i < m_size; ++i ) {
                if( m_holder[i] == res ) {
                    delete m_holder[i];
                    m_holder[i] = 0;
                    return true;
                }
            }
            return false;
        }

        void DestroyAll( )
        {
            for( int i = 0; i < m_size; ++i ) {
                if( m_holder[i] ) {
                    delete m_holder[i];
                    m_holder[i] = 0;
                }
            }
        }

        INLINE int GetSize( )
        {
            return m_size;
        }

    private:

        T** m_holder;
        int m_size;

    };

}
