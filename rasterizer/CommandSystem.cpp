#include "precompiled.h"
#include "CommandSystem.h"

namespace Rasterizer2 {

    bool CommandSystem::RegisterStateVar( StateVar *var )
    {
        if( !var ) {
            return false;
        }

        std::pair< std::string, StateVar* > pair( std::string( var->GetName( ) ), var );
        auto result = m_stateVars.insert( pair );
        return result.second;
    }

    int CommandSystem::RegisterStateVars( StateVar **vars, int varCount )
    {
        int successCnt = 0;

        for( int i = 0; i < varCount; ++i ) {
            if( vars[i] ) {
                std::pair< std::string, StateVar* > pair( std::string( vars[i]->GetName( ) ), vars[i] );
                auto result = m_stateVars.insert( pair );
                if( result.second ) {
                    ++successCnt;
                }
            }
        }

        return successCnt;
    }

    bool CommandSystem::RegisterStateCmd( StateCmd *cmd )
    {
        if( !cmd ) {
            return false;
        }

        std::pair< std::string, StateCmd* > pair( std::string( cmd->GetName( ) ), cmd );
        auto result = m_stateCmds.insert( pair );
        return result.second;
    }

    int CommandSystem::RegisterStateCmds( StateCmd **cmds, int cmdCount )
    {
        int successCnt = 0;

        for( int i = 0; i < cmdCount; ++i ) {
            if( cmds[i] ) {
                std::pair< std::string, StateCmd* > pair( std::string( cmds[i]->GetName( ) ), cmds[i] );
                auto result = m_stateCmds.insert( pair );
                if( result.second ) {
                    ++successCnt;
                }
            }
        }

        return successCnt;
    }
}
