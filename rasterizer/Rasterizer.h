#pragma once

#include "Mesh.h"
#include "ICommandSystem.h"

namespace Rasterizer2
{
    class Rasterizer
    {

    public:

        Rasterizer( IState *state );
        ~Rasterizer( );
 
        bool Initialize( );
        void SetupCommandSystem( ICommandSystem *cmdSystem );

        Mesh *CreateCube( float edgeLength, Material front, Material back, Material left, Material right, Material top, Material bottom );
        Mesh *CreateQuad( );
        void Update( );
        void MouseCapture( );
        void MouseRelease( );
        void Frame( );
        void Quit( );
        INLINE int GetWindowWidth( ) const
        {
            return m_viewportWidth.GetIntValue( );
        }
        INLINE int GetWindowHeight( ) const
        {
            return m_viewportHeight.GetIntValue( );
        }

        unsigned char *keyboard;
        unsigned int mouse;
        bool isMouseCaptured;
        unsigned int wndFlags;
        int mouse_x;
        int mouse_y;
        int numkeys;
        View view;

    private:

        IState *m_state;
        StateVar m_viewportWidth;
        StateVar m_viewportHeight;
        StateVar m_mode;
        StateVar m_vsync;
        SDL_Window* m_wnd;
        SDL_GLContext m_context;
        GLuint m_program;

    };
}
