#pragma once

#include "ICommandSystem.h"
#include "Script.h"

namespace Rasterizer2
{
    class CommandSystem : public ICommandSystem
    {

    public:

        bool RegisterStateVar( StateVar *var );
        int RegisterStateVars( StateVar **vars, int varCount );
        bool RegisterStateCmd( StateCmd *cmd );
        int RegisterStateCmds( StateCmd **cmds, int cmdCount );

        INLINE StateVar *GetStateVar( const char *name )
        {
            auto it = m_stateVars.find( std::string( name ) );
            return it != m_stateVars.end( ) ? ( *it ).second : nullptr;
        }

        INLINE const StateVar *GetStateVar( const char *name ) const
        {
            auto it = m_stateVars.find( std::string( name ) );
            return it != m_stateVars.end( ) ? ( const StateVar* )( ( *it ).second ) : nullptr;
        }

        INLINE bool StateVarExists( const char *name ) const
        {
            return m_stateVars.find( std::string( name ) ) != m_stateVars.end( );
        }

        INLINE StateCmd *GetStateCmd( const char *name )
        {
            auto it = m_stateCmds.find( std::string( name ) );
            return it != m_stateCmds.end( ) ? ( *it ).second : nullptr;
        }

        INLINE const StateCmd *GetStateCmd( const char *name ) const
        {
            auto it = m_stateCmds.find( std::string( name ) );
            return it != m_stateCmds.end( ) ? ( const StateCmd* )( ( *it ).second ) : nullptr;
        }

        INLINE bool StateCmdExists( const char *name ) const
        {
            return m_stateCmds.find( std::string( name ) ) != m_stateCmds.end( );
        }

    private:

        std::unordered_map< std::string, StateVar* > m_stateVars;
        std::unordered_map< std::string, StateCmd* > m_stateCmds;

    };
}