#ifdef _WIN32
#include <Windows.h>
#else
#include <pthread.h>
#endif
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cstring>
#include <cassert>
#include <limits>
#include <memory>
#include <string>
#include <vector>
#include <list>
#include <queue>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

#define SHADER_LOCATION "data/shaders/"
#define IMAGE_LOCATION "data/images/"
#define GEOMETRY_LOCATION "data/geometry/"

#ifdef _WIN32
#define INLINE __forceinline
#else
#define INLINE inline
#endif

#define BIT_FLAG(x) (1<<x)

namespace Rasterizer2
{
    typedef unsigned long long uint64;
    typedef unsigned int uint;
    typedef unsigned char byte;

    template< typename T >
    INLINE void Swap( T& a, T& b )
    {
        T t = a;
        a = b;
        b = t;
    }

    INLINE int FileLength( FILE *f )
    {
        int pos, end;

        pos = ftell( f );
        fseek( f, 0, SEEK_END );
        end = ftell( f );
        fseek( f, pos, SEEK_SET );

        return end;
    }

    INLINE const char *FileExtension( const char *path )
    {
        const char *pos = 0;
        while( *path ) {
            if( *path == '.' ) {
                pos = path + 1;
            }
            ++path;
        }
        return pos ? pos : path;
    }

    INLINE void StripFileExtension( char *path )
    {
        char *p = path + strlen( path ) - 1;
        while( p != path && *p != '.' ) {
            --p;
        }
        if( p != path ) {
            *p = 0;
        }
    }

    double Counter( void );
}

#include <SDL2/SDL.h>
#include <IL/il.h>

#include "OpenGL.h"
#include "Math.h"
#include "Memory.h"
#include "Delegate.h"
#include "DebugOutput.h"
