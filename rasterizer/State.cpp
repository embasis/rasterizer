#include "precompiled.h"
#include "State.h"
#include "Texture.h"

namespace Rasterizer2 {

    State::State( ) : m_r( ( IState* )this ), m_scene( DemoScene( m_r, ( IState *)this ) )
    {
    }

    State::~State( )
    {
    }

    bool State::InitRasterizer( )
    {
        if( !m_r.Initialize( ) ) {
            DebugOutput::Printf( "Rasterizer initialization failed" );
            return false;
        }
        return true;
    }

    bool State::LaunchDemoScene( )
    {
        if( InitRasterizer( ) ) {
            return m_scene.Create( );
        }
        return false;
    }

    void State::SetupCommandSystem( ICommandSystem *cmdSystem )
    {
        m_r.SetupCommandSystem( cmdSystem );
        m_scene.SetupCommandSystem( cmdSystem );
    }

}