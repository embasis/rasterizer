#include "precompiled.h"
#include "OBJFile.h"

namespace Rasterizer2
{
    OBJFile::OBJFile( IState *state ) : m_state( state )
    {
    }

    OBJFile::~OBJFile( )
    {
        Clear( );
    }

    void OBJFile::Clear( )
    {
        m_pos.clear( );
        m_uv.clear( );
        m_normal.clear( );
        m_vertexes.clear( );
        for( MTLFile *f : m_mtl ) {
            delete f;
        }
        for( Face *f : m_faces ) {
            delete f;
        }
        m_mtl.clear( );
        m_faces.clear( );
    }

    bool OBJFile::Parse( FILE *file )
    {
        const int MAX_STR_LENGTH = 2047, MAX_TOKENS = MAX_STR_LENGTH / 2 + 2;
        char str[MAX_STR_LENGTH+1];

        Clear( );

        int curSmoothingGroup = 0;
        char curMaterial[MTLFile::MAX_MATERIAL_NAME_LENGTH];
        *curMaterial = 0;

        for( int line = 1; fgets( str, MAX_STR_LENGTH + 1, file ); ++line ) {
            // delete \n \r \t symbols
            for( int i = 0; str[i]; ++i ) {
                if( str[i] == '\n' || str[i] == '\r' || str[i] == '\t' ) {
                    str[i] = ' ';
                }
            }

            // skip comments
            if( *str == '#' ) {
                continue;
            }

            char *tokens[MAX_TOKENS];
            int tokenCnt = 0;

            tokens[0] = strtok( str, " " );
            while( tokens[tokenCnt] != 0 ) {
                tokens[++tokenCnt] = strtok( 0, " " );
            }
            
            if( tokenCnt ) {
                Vec3 vec;
                int coordCnt = 0;
                if( !strcmp( tokens[0], "v" ) || !strcmp( tokens[0], "vn" ) ) {
                    coordCnt = 3;
                } else if( !strcmp( tokens[0], "vt" ) ) {
                    coordCnt = 2;
                } else if( !strcmp( tokens[0], "f" ) ) {
                    if( tokenCnt - 1 < 3 ) {
                        DebugOutput::Printf( "line %d: expected at least 3 tokens, %d found\n", line, tokenCnt - 1 );
                        return false;
                    }
                    std::unique_ptr< Face > face( new Face( ) );
                    face->vertIndex = ( int )m_vertexes.size( );
                    for( int i = 1; i < tokenCnt; ++i ) {
                        Vertex vert;
                        int read;
                        if( strstr( tokens[i], "//" ) ) {
                            read = sscanf( tokens[i], "%d//%d", &vert.posIndex, &vert.normalIndex ) == 2 ? 2 : 0;
                        } else if( strstr( tokens[i], "/" ) ) {
                            read = sscanf( tokens[i], "%d/%d/%d", &vert.posIndex, &vert.uvIndex, &vert.normalIndex ) == 3 ? 3 : 0;
                        } else {
                            read = sscanf( tokens[i], "%d", &vert.posIndex );
                        }
                        if( read ) {
                            // handle negative references
                            if( vert.posIndex < 0 ) {
                                vert.posIndex = m_pos.size( ) + vert.posIndex + 1;
                            }
                            if( vert.uvIndex < 0 ) {
                                vert.uvIndex = m_uv.size( ) + vert.uvIndex + 1;
                            }
                            if( vert.normalIndex < 0 ) {
                                vert.normalIndex = m_normal.size( ) + vert.normalIndex + 1;
                            }
                            // check validity of final indexes
                            if( vert.posIndex <= 0 || vert.posIndex > ( int )m_pos.size( ) ||
                                ( read == 3 && ( vert.uvIndex <= 0 || vert.uvIndex > ( int )m_uv.size( ) ) ) ||
                                ( ( read == 2 || read == 3 ) && ( vert.normalIndex <= 0 || vert.normalIndex > ( int )m_normal.size( ) ) ) ) {
                                DebugOutput::Printf( "line %d: bad face indexes \"%s\"\n", line, tokens[i] );
                                return false;
                            }
                            m_vertexes.push_back( vert );
                        } else {
                            DebugOutput::Printf( "line %d: wrong face statement element: \"%s\", ignored\n", line, tokens[i] );
                        }
                    }
                    strcpy( face->materialName, curMaterial );
                    face->vertCnt = ( int )m_vertexes.size( ) - face->vertIndex;
                    face->smoothingGroup = curSmoothingGroup;
                    m_faces.push_back( face.release( ) );
                } else if( !strcmp( tokens[0], "s" ) ) {
                    if( !( tokenCnt - 1 ) || ( strcmp( tokens[1], "off" ) && !sscanf( tokens[1], "%d", &curSmoothingGroup ) ) ) {
                        DebugOutput::Printf( "line %d: expected an integer or \"off\", none found, ignored\n", line );
                    }
                    if( !strcmp( tokens[1], "off" ) ) {
                        curSmoothingGroup = 0;
                    }
                } else if( !strcmp( tokens[0], "mtllib" ) ) {
                    if( !( tokenCnt - 1 ) ) {
                        DebugOutput::Printf( "line %d: expected at least one token, none found\n", line );
                        return false;
                    }
                    int curToken = 1;
                    while( curToken < tokenCnt ) {
                        // MTL file names can contain spaces, so we assume that every specified file name ends with .mtl
                        int startToken = curToken;
                        while( curToken < tokenCnt && !strstr( tokens[curToken], ".mtl" ) ) {
                            ++curToken;
                        }
                        if( curToken == tokenCnt ) {
                            DebugOutput::Printf( "line %d: bad file name\n", line );
                            return false;
                        }
                        int endTokenLen = strlen( tokens[curToken] );
                        for( char *itk = tokens[startToken]; itk != tokens[curToken] + endTokenLen; ++itk ) {
                            if( !*itk ) {
                                *itk = ' ';
                            }
                        } 
                        MTLFile *mtl = new MTLFile( m_state ); // beware of leaks
                        if( !mtl->LoadFile( tokens[startToken] ) ) {
                            DebugOutput::Printf( "line %d: failed to load specified material library \"%s\"\n", line, tokens[startToken] );
                            delete mtl;
                            return false;
                        }
                        m_mtl.push_back( mtl );
                        ++curToken;
                    }
                } else if( !strcmp( tokens[0], "usemtl" ) ) {
                    if( !( tokenCnt - 1 ) ) {
                        DebugOutput::Printf( "line %d: expected 1 token, 0 found\n", line );
                        return false;
                    }
                    if( strlen( tokens[1] ) > MTLFile::MAX_MATERIAL_NAME_LENGTH ) {
                        DebugOutput::Printf( "line %d: material name \"%s\" is too long\n", line, tokens[1] );
                        return false;
                    }
                    strcpy( curMaterial, tokens[1] );
                } else {
                    DebugOutput::Printf( "line %d: unrecognized keyword \"%s\", ignored\n", line, tokens[0] );
                }

                if( coordCnt ) {
                    if( tokenCnt - 1 < coordCnt ) {
                        DebugOutput::Printf( "line %d: expected %d tokens, %d found\n", line, coordCnt, tokenCnt - 1 );
                        return false;
                    }
                    int read = 0;
                    for( int i = 0; i < coordCnt; ++i ) {
                        read += sscanf( tokens[i + 1], "%f", &vec[i] );
                    }
                    if( read < coordCnt ) {
                        DebugOutput::Printf( "line %d: expected %d floats, %d found\n", line, coordCnt, read );
                        return false;
                    }
                    if( !strcmp( tokens[0], "v" ) ) {
                        m_pos.push_back( vec );
                    } else if( !strcmp( tokens[0], "vn" ) ) {
                        m_normal.push_back( vec );
                    } else {
                        m_uv.push_back( vec );
                    }
                }
            }
        }

        // sort faces by used material
        std::sort( m_faces.begin( ), m_faces.end( ), []( Face *l, Face *r ) {
            return std::lexicographical_compare( l->materialName, l->materialName + strlen( l->materialName ),
                r->materialName, r->materialName + strlen( r->materialName ) );
        } );

        return true;
    }

    bool OBJFile::ParseFromMemory( const void *buffer )
    {
        return false;
    }

    Mesh *OBJFile::MakeMesh( ) const
    {
        DebugOutput::Printf( "Creating mesh from \"%s\"\n", GetPath( ) );

        int curVert = 0, curInd = 0, curMatInd = 0, trCount = 0, matCount = 0;
        for( MTLFile *f : m_mtl ) {
            matCount += f->GetMaterialsCount( );
        }
        for( Face *f : m_faces ) {
            trCount += f->vertCnt - 2;
        }
        std::unique_ptr< int[] > indexes( new int[trCount * 3] ), groupBorders( new int[matCount] );
        std::unique_ptr< Rasterizer2::Vertex[] > vertexes( new Rasterizer2::Vertex[m_vertexes.size( )] );
        std::unique_ptr< Material[] > materials( new Material[matCount] );
//        std::map< Vertex, int, std::less< Vertex >, MemoryPoolAllocator< std::pair< const Vertex, int >, 1U << 28 > > faceVertIndexes;
        std::map< Vertex, int > faceVertIndexes;
        char curMaterial[MTLFile::MAX_MATERIAL_NAME_LENGTH+1];
        *curMaterial = 0;

        for( Face *f : m_faces ) {
            if( strcmp( f->materialName, curMaterial ) ) {
                if( *curMaterial ) {
#ifdef _DEBUG
                    assert( curMatInd < matCount );
#endif
                    groupBorders[curMatInd++] = curInd - 1;
                }
                strcpy( curMaterial, f->materialName );
                for( MTLFile *mf : m_mtl ) {
                    if( mf->Contains( curMaterial ) ) {
                        if( !mf->CreateMaterial( curMaterial, materials[curMatInd] ) ) {
                            DebugOutput::Printf( "MakeMesh( ): failed to load texture for material \"%s\"\n", curMaterial );
                            return nullptr;
                        }
                        break;
                    }
                }
            }
            int firstVertIndex = -1, prevVertIndex = -1;
            for( int i = 0; i < f->vertCnt; ++i ) {
#ifdef _DEBUG
                assert( curInd < trCount * 3 );
                assert( curVert < ( int )m_vertexes.size( ) );
#endif
                const Vertex &v = m_vertexes[f->vertIndex + i];
                int curIndex;
                if( !faceVertIndexes.count( v ) ) {
                    vertexes[curVert].pos = m_pos[v.posIndex - 1];
                    if( v.uvIndex ) {
                        vertexes[curVert].uv = m_uv[v.uvIndex - 1];
                    }
                    if( v.normalIndex ) {
                        vertexes[curVert].normal = m_normal[v.normalIndex - 1];
                    }
                    curIndex = curVert++;
                    faceVertIndexes[v] = curIndex;
                } else {
                    curIndex = faceVertIndexes[v];
                }
                if( firstVertIndex != -1 ) {
                    if( prevVertIndex != -1 ) {
                        indexes[curInd++] = firstVertIndex;
                        indexes[curInd++] = prevVertIndex;
                        indexes[curInd++] = curIndex;
                    }
                    prevVertIndex = curIndex;
                } else {
                    firstVertIndex = curIndex;
                }
            }
        }
        groupBorders[curMatInd++] = curInd - 1;

        Mesh *mesh = m_state->GetMeshesManager( ).Create( );
        mesh->SetVertexData( vertexes.release( ), curVert );
        mesh->SetIndexData( indexes.release( ), curInd, materials.release( ), curMatInd, groupBorders.release( ) );

        DebugOutput::Printf( "Mesh was successfully created\n" );

        return mesh;
    }

    const char *OBJFile::GetFileType( ) const
    {
        return "OBJFile";
    }

    const char *OBJFile::GetFileLocation( ) const
    {
        return GEOMETRY_LOCATION;
    }
}