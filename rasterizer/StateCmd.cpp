#include "precompiled.h"
#include "StateCmd.h"

namespace Rasterizer2 {
    const StateCmd &StateCmd::operator()( ICommandSystem *cmdSystem, IState *state, int argCount, ... ) const
    {
        int actualArgCount = std::min( argCount, m_argCount );

        va_list args;
        va_start( args, argCount );

        ArgBasic basicArgs[actualArgCount];
        for( int i = 0; i < actualArgCount; ++i ) {
            ArgBasic &arg = basicArgs[i];

            switch( m_argDescs[i].internalType ) {
                case ArgDesc::INT:
                    arg.i = va_arg( args, int );
                    break;
                case ArgDesc::UINT:
                    arg.ui = va_arg( args, unsigned int );
                    break;
                case ArgDesc::FLOAT:
                    arg.f = ( float )va_arg( args, double );
                    break;
                case ArgDesc::STRING:
                    arg.char_ptr = va_arg( args, char* );
                    break;
                default:
                    DebugOutput::Printf( "\"%s\" command recieved argument of unsupported type at \"%d\" index, type value: \"%d\"\n", m_name, i, m_argDescs[i].internalType );
            }
        }

        va_end( args );

        m_cmdFunc( cmdSystem, state, basicArgs, actualArgCount );

        return ( const StateCmd& )*this;
    }

    const StateCmd &StateCmd::operator()( ICommandSystem *cmdSystem, IState *state, ArgBasic *basicArgs, int basicArgsCnt ) const
    {
        m_cmdFunc( cmdSystem, state, basicArgs, std::min( basicArgsCnt, m_argCount ) );

        return ( const StateCmd& )*this;
    }
}