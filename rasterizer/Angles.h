#pragma once

namespace Rasterizer2
{
    class Angles
    {

    public:

	    float pitch, yaw, roll;

	    INLINE Angles( ) : pitch( 0.f ), yaw( 0.f ), roll( 0.f )
	    {
	    }
	    INLINE Angles( float pitch, float yaw, float roll )
	    {
		    Angles::pitch = pitch;
		    Angles::yaw = yaw;
		    Angles::roll = roll;
	    }
	    INLINE explicit Angles( const Vec3 &v )
	    {
		    pitch = v.x;
		    yaw = v.y;
		    roll = v.z;
	    }
	    INLINE Angles operator+( const Angles &a ) const
	    {
		    return Angles( pitch + a.pitch, yaw + a.yaw, roll + a.roll );
	    }
	    INLINE Angles operator-( const Angles &a ) const
	    {
		    return Angles( pitch - a.pitch, yaw - a.yaw, roll - a.roll );
	    }
	    INLINE Angles& operator+=( const Angles &a )
	    {
		    pitch += a.pitch;
		    yaw += a.yaw;
		    roll += a.roll;
		    return *this;
	    }
	    INLINE Angles& operator-=( const Angles &a )
	    {
		    pitch -= a.pitch;
		    yaw -= a.yaw;
		    roll -= a.roll;
		    return *this;
	    }
        INLINE bool operator==( const Angles &a ) const
        {
            return pitch == a.pitch && yaw == a.yaw && roll == a.roll;
        }
        INLINE bool operator!=( const Angles &a ) const
        {
            return !( *this == a );
        }
        //|forward.x forward.y forward.z|
        //|right.x   right.y   right.z  |
        //|up.x      up.y      up.z     |
        void ToVectors( Mat3x3 &vec, bool forward, bool right = false, bool up = false ) const;
	    INLINE void Zero( )
	    {
		    pitch = 0.f;
		    yaw = 0.f;
		    roll = 0.f;
	    }

    };
}