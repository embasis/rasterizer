#include "precompiled.h"

namespace Rasterizer2
{
    float Mat4x4::Determinant( ) const
    {
	    float det3344 = m_v[2].z * m_v[3].w - m_v[2].w * m_v[3].z;
	    float det3244 = m_v[2].y * m_v[3].w - m_v[2].w * m_v[3].y;
	    float det3243 = m_v[2].y * m_v[3].z - m_v[2].z * m_v[3].y;
	    float det3144 = m_v[2].x * m_v[3].w - m_v[2].w * m_v[3].x;
	    float det3143 = m_v[2].x * m_v[3].z - m_v[2].z * m_v[3].x;
	    float det3142 = m_v[2].x * m_v[3].y - m_v[2].y * m_v[3].x;
	
	    float det1 = m_v[1].y * det3344 - m_v[1].z * det3244 + m_v[1].w * det3243;
	    float det2 = m_v[1].x * det3344 - m_v[1].z * det3144 + m_v[1].w * det3143;
	    float det3 = m_v[1].x * det3244 - m_v[1].y * det3144 + m_v[1].w * det3142;
	    float det4 = m_v[1].x * det3243 - m_v[1].y * det3143 + m_v[1].z * det3142;

	    return m_v[0].x * det1 - m_v[0].y * det2 + m_v[0].z * det3 - m_v[0].w * det4;
    }

    float Mat4x4::DeterminantSlow( ) const
    {
	    Mat3x3 minor;
	    minor[0].x = m_v[1].y; minor[0].y = m_v[1].z; minor[0].z = m_v[1].w;
	    minor[1].x = m_v[2].y; minor[1].y = m_v[2].z; minor[1].z = m_v[2].w;
	    minor[2].x = m_v[3].y; minor[2].y = m_v[3].z; minor[2].z = m_v[3].w;
	    float res = m_v[0].x * minor.Determinant( );
	    minor[0].x = m_v[1].x; minor[0].y = m_v[1].z; minor[0].z = m_v[1].w;
	    minor[1].x = m_v[2].x; minor[1].y = m_v[2].z; minor[1].z = m_v[2].w;
	    minor[2].x = m_v[3].x; minor[2].y = m_v[3].z; minor[2].z = m_v[3].w;
	    res -= m_v[0].y * minor.Determinant( );
	    minor[0].x = m_v[1].x; minor[0].y = m_v[1].y; minor[0].z = m_v[1].w;
	    minor[1].x = m_v[2].x; minor[1].y = m_v[2].y; minor[1].z = m_v[2].w;
	    minor[2].x = m_v[3].x; minor[2].y = m_v[3].y; minor[2].z = m_v[3].w;
	    res += m_v[0].z * minor.Determinant( );
	    minor[0].x = m_v[1].x; minor[0].y = m_v[1].y; minor[0].z = m_v[1].z;
	    minor[1].x = m_v[2].x; minor[1].y = m_v[2].y; minor[1].z = m_v[2].z;
	    minor[2].x = m_v[3].x; minor[2].y = m_v[3].y; minor[2].z = m_v[3].z;
	    return res - m_v[0].w * minor.Determinant( );
    }
}