#include "precompiled.h"

namespace Rasterizer2
{
    double Counter( )
    {
        static uint64 tickBase, ticksPerMillisecond;
        static bool initialized = false;
        uint64 curTime;
#ifdef _WIN32
        if( !initialized ) {
            QueryPerformanceFrequency( ( PLARGE_INTEGER ) &ticksPerMillisecond );
            QueryPerformanceCounter( ( PLARGE_INTEGER ) &tickBase );
            ticksPerMillisecond /= 1000;
            initialized = true;
        }
        QueryPerformanceCounter( ( PLARGE_INTEGER ) &curTime );
#else
        const uint64 nanosecInSec = 1000000000;
        if( !initialized ) {
            timespec tres, tbase;
            clock_getres( CLOCK_MONOTONIC, &tres );
            clock_gettime( CLOCK_MONOTONIC, &tbase );
            ticksPerMillisecond = ( tres.tv_sec * nanosecInSec + tres.tv_nsec ) * ( nanosecInSec / 1000 );
            tickBase = tbase.tv_sec * nanosecInSec + tbase.tv_nsec;
            initialized = true;
        }
        timespec tcurTime;
        clock_gettime( CLOCK_MONOTONIC, &tcurTime );
        curTime = tcurTime.tv_sec * nanosecInSec + tcurTime.tv_nsec;
#endif
        return ( curTime - tickBase ) * 1.0 / ticksPerMillisecond;
    }
}
