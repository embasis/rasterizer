#include "precompiled.h"
#include "DemoScene.h"

namespace {
    DemoScene scene;
}

int WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR cmdLine, int )
{
    srand( ( unsigned int )time( NULL ) );
    float lastTime = ( float )Counter( );
    float lastFps = lastTime;
    
    DebugOutput::CreateConsole( hInst );
    DebugOutput::MoveToCorner( );
    DebugOutput::ShowConsole( SW_SHOWDEFAULT );

    if( !scene.Create( ) ) {
        return 0;
    }

    while( !( GetAsyncKeyState( VK_ESCAPE ) & 1 ) ) {
        MSG msg;
        if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) ) {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        } else {
            float curTime = ( float )Counter( );
            scene.Frame( curTime - lastTime );

            if( curTime - lastFps > 250.f ) {
                DebugOutput::SetTopbufferText( "FPS: %d\n", int( 1000.f / ( curTime - lastTime + 0.00001 ) ) );
                //DebugOutput::Printf( "%f\n", curTime - lastTime );
                lastFps = curTime;
            }

            scene.UpdateScreen( );
            lastTime = curTime;
        }
    }
    
    return 0;
}
