#pragma once

#include "SceneObject.h"

namespace Rasterizer2
{
    class Scene
    {

    public:

        Scene( );
        ~Scene( );

        INLINE SceneObject *GetRoot( ) const
        {
            return m_root;
        }

        INLINE void AddChild( SceneObject *parent )
        {
        }

    private:

        SceneObject *m_root;

    };
}