#pragma once

#include "File.h"
#include "Material.h"

namespace Rasterizer2
{
    class MTLFile : public File
    {

    public:

        static const int MAX_MATERIAL_NAME_LENGTH = 64;
        static const int MAX_MAP_PATH_LENGTH = MAX_PATH - MAX_MATERIAL_NAME_LENGTH - 2;

        MTLFile( IState *state );
        ~MTLFile( );

        void Clear( );
        bool Contains( const char *matName ) const;
        bool CreateMaterial( const char *matName, Rasterizer2::Material &dest ) const;
        int GetMaterialsCount( ) const;

    private:

        enum IlluminationModel
        {
            NO_ILLUMINATION = -1,
            COLOR_ON_AMBIENT_OFF,
            COLOR_ON_AMBIENT_ON,
            HIGHLIGHT_ON,
            REFLECTION_ON_RAYTRACE_ON,
            GLASS_ON_RAYTRACE_ON,
            FRESNEL_ON_RAYTRACE_ON,
            REFRACTION_ON_FRESNEL_OFF_RAYTRACE_ON,
            REFRACTION_ON_FRESNEL_ON_RAYTRACE_ON,
            REFLECTION_ON_RAYTRACE_OFF,
            GLASS_ON_RAYTRACE_OFF,
            CAST_SHADOWS
        };

        struct Material
        {
            INLINE Material( ) : illum( NO_ILLUMINATION ), dissolve( 1.f ), halo( 1.0 ), specularExp( 0.f ), sharpness( 60.f ),
                opticalDensity( 1.f )
            {
                *ambientMap = 0;
                *diffuseMap = 0;
                *specularMap = 0;
                *bumpMap = 0;
            }

            char name[MAX_MATERIAL_NAME_LENGTH+1];
            Vec4 ambientColor;
            Vec4 diffuseColor;
            Vec4 specularColor;
            Vec4 transmissionFilter;
            IlluminationModel illum;
            float dissolve;
            float halo;
            float specularExp;
            float sharpness;
            float opticalDensity;
            char ambientMap[MAX_MAP_PATH_LENGTH+1];
            char diffuseMap[MAX_MAP_PATH_LENGTH+1];
            char specularMap[MAX_MAP_PATH_LENGTH+1];
            char bumpMap[MAX_MAP_PATH_LENGTH+1];
        };

        std::vector< Material* > m_materials;
        IState *m_state;

        const char *GetFileType( ) const;
        const char *GetFileLocation( ) const;

        bool Parse( FILE *file );
        bool ParseFromMemory( const void *buffer );

    };
}