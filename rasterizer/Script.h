#pragma once

#include "Resource.h"
#include "MemoryPool.h"

namespace Rasterizer2
{
    class Script
    {

    public:

        static const int SCRIPT_NAME_MAX_LENGTH = 64;
        static const int MAX_SCRIPT_SIZE = 128 * 1024; // 128 kilobytes
        static const int COMMAND_VECTORS_CNT = 1024;

        Script( const char *name, ICommandSystem *cmdSystem, ResourceHolder< Script > &scriptsManager );
        ~Script( );

        struct Argument
        {
            ArgBasic basicArg;
            StateCmd::ArgDesc::Type type;
            int slotNum;
            bool slotSubstitution;
            bool cmdsSubstitution;
            std::vector< Command > *cmds;
        };

        struct Command
        {
            INLINE Command( ) : stateCmd( nullptr ), argCnt( 0 )
            {
            }

            StateCmd *stateCmd;
            Argument arguments[StateCmd::MAX_ARG_COUNT];
            int argCnt;
        };

        bool Parse( const char *text );

        INLINE const char *GetName( ) const
        {
            return m_name;
        }

        INLINE const std::vector &GetCommands( ) const
        {
            return m_commands;
        }

    private:

        static const int RESERVED_COMMANDS_CNT = 8;
        static const int CHAR_BUF_SIZE = 3;
        static const int IGNORE_DELIM_COUNT = 4;

        enum ParseFlags
        {
            ACCUM_TOKEN = 1,
            ACCUM_DOUBLE_QUOTED_TOKEN = BIT_FLAG( 1 ),
            ACCUM_CMD_OR_DECL_NAME = BIT_FLAG( 2 ),
            IGNORING = BIT_FLAG( 3 )
        };

        struct State
        {
            INLINE State( ) :
                ignoreDelimsCnt( 0 ),
                charBuf( { 0, 0 } ),
                ignoreCharBuf( { 0, 0 } ),
                accumBufIndex( 0 ),
                parseFlags( ACCUM_TOKEN & ACCUM_CMD_OR_DECL_NAME ),
                line( 1 ),
                col( 1 )
            {
                memset( ignoreDelims, 0, IGNORE_DELIM_COUNT * CHAR_BUF_SIZE * sizeof( char ) );
            }

            char ignoreDelims[IGNORE_DELIM_COUNT][CHAR_BUF_SIZE]; 
            int ignoreDelimsCnt;
            char charBuf[CHAR_BUF_SIZE];
            char ignoreCharBuf[CHAR_BUF_SIZE];
            char accumBuf[MAX_SCRIPT_SIZE+1];
            int accumBufIndex;
            uint parseFlags;
            int line;
            int col;

            void SetIgnoring( const char **delims, int delimCount );
            void ClearIgnoring( );
            void UpdateCharBuf( char *buf, char c );
            void UpdateLineCol( );
            const char *CheckDelims( const char *buf, const char **delims, int delimCount ) const;
            bool TestBuf( const char *buf, const char *testSeq ) const;
            INLINE char CurChar( ) const
            {
                return charBuf[CHAR_BUF_SIZE - 1];
            }
            INLINE void AccumBufPush( char c )
            {
#ifdef _DEBUG
                assert( accumBufIndex < MAX_SCRIPT_SIZE || ( accumBufIndex == MAX_SCRIPT_SIZE && c == 0 ) );
#endif
                accumBuf[accumBufIndex++] = c;
            }
            INLINE void AccumBufPop( )
            {
                if( accumBufIndex > 0 ) {
                    --accumBufIndex;
                }
            }
            INLINE void AccumBufReset( )
            {
                accumBufIndex = 0;
            }
        };

        struct NamedDeclaration
        {
            char *strValue;
            StateCmd::ArgBasic value;
            StateCmd::ArgDesc::Type type;
        };

        struct SemanticState
        {
            INLINE SemanticState( )
            {
            }

            enum TokenType
            {
                CMD_OR_DECL_NAME,
                END_TOKEN_OR_EQUALS, // end tokens are '\n' and ';'

            };

            std::vector< Command > cmds;
            char blankToken[MAX_SCRIPT_SIZE+1]; // cmd name or named decl name( if next token is '=' )
        };

        void ProcessToken( State &state );
        void PrintError( const State &state, const char *msg, ... ) const;

        std::vector< SemanticState > m_stateStack;
        std::unordered_map< std::string, NamedDeclaration > m_namedDeclarations;

        char m_name[SCRIPT_NAME_MAX_LENGTH+1];
        std::vector< Command > m_commands;
        ResourceHolder< Script > &m_scriptsManager;
        ICommandSystem *m_cmdSystem;
        MemoryPool m_stringPool;
        MemoryPool m_commandVectorsPool;

    };
}