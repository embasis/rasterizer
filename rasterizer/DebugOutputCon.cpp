#include "precompiled.h"
#include "DebugOutput.h"

namespace Rasterizer2
{
    void DebugOutput::CreateInputThread( )
    {
        
    }

    void DebugOutput::ShowConsole( int showCmd )
    {
    }

    void DebugOutput::Printf( const char *text, ... )
    {
        char msg[MAX_MESSAGE_LENGTH+1];
        va_list args;

        va_start( args, text );
        vsprintf( msg, text, args );
        va_end( args );

        printf( "%s", msg );
    }

    void DebugOutput::FatalError( const char *text, ... )
    {
        char msg[MAX_MESSAGE_LENGTH+1];
        va_list args;

        va_start( args, text );
        vsprintf( msg, text, args );
        va_end( args );

        printf( "%s", msg );
    }

    void DebugOutput::SetTopbufferText( const char *text, ... )
    {
    }

    void DebugOutput::MoveToCorner( void )
    {
    }
}
