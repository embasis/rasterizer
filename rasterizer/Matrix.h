// Matrix.h - when u multiply matrix and vector, vector is considered to be a column, but when
// u access vector as a part of matrix that would be a row

#pragma once 

namespace Rasterizer2
{
    const float MAT_EPSILON = 0.000001f;

    class Mat2x2
    {

    public:

        INLINE Mat2x2( )
        {
            Zero( );
        }
        explicit INLINE Mat2x2( const float xx, const float xy,
                                const float yx, const float yy )
        {
            m_v[0].x = xx;
            m_v[0].y = xy;
            m_v[1].x = yx;
            m_v[1].y = yy;
        }
	    // returns row!
        INLINE const Vec2& operator[]( int index ) const
        {
            return m_v[index];
        }
	    // returns row!
        INLINE Vec2& operator[]( int index )
        {
            return m_v[index];
        }
        INLINE Mat2x2 operator+( const Mat2x2 &m ) const
        {
            return Mat2x2( m_v[0].x+m[0].x, m_v[0].y+m[0].y, m_v[1].x+m[1].x, m_v[1].y+m[1].y );
        }
        INLINE Mat2x2 operator-( const Mat2x2 &m ) const
        {
            return Mat2x2( m_v[0].x-m[0].x, m_v[0].y-m[0].y, m_v[1].x-m[1].x, m_v[1].y-m[1].y );
        }
        INLINE Mat2x2 operator*( float a ) const
        {
            return Mat2x2( m_v[0].x*a, m_v[0].y*a, m_v[1].x*a, m_v[1].y*a );
        }
        INLINE Mat2x2 operator*( const Mat2x2 &m ) const
        {
            return Mat2x2( m_v[0].x*m[0].x + m_v[0].y*m[1].x,
                           m_v[0].x*m[0].y + m_v[0].y*m[1].y,
                           m_v[1].x*m[0].x + m_v[1].y*m[1].x,
                           m_v[1].x*m[0].y + m_v[1].y*m[1].y );
        }
        INLINE Vec2 operator*( const Vec2 &v ) const
        {
            return Vec2( v.x*m_v[0].x + v.y*m_v[1].x, v.x*m_v[0].y + v.y*m_v[1].y );
        }
        INLINE Mat2x2 operator-( ) const
        {
            return Mat2x2( -m_v[0].x, -m_v[0].y, -m_v[1].x, -m_v[1].y );
        }
        INLINE Mat2x2& operator+=( const Mat2x2 &m )
        {
            m_v[0].x += m[0].x;
            m_v[0].y += m[0].y;
            m_v[1].x += m[1].x;
            m_v[1].y += m[1].y;
            return *this;
        }
        INLINE Mat2x2& operator-=( const Mat2x2 &m )
        {
            m_v[0].x -= m[0].x;
            m_v[0].y -= m[0].y;
            m_v[1].x -= m[1].x;
            m_v[1].y -= m[1].y;
            return *this;
        }
        INLINE Mat2x2& operator*=( float a )
        {
            m_v[0].x *= a;
            m_v[0].y *= a;
            m_v[1].x *= a;
            m_v[1].y *= a;
            return *this;
        }
        INLINE Mat2x2& operator*=( Mat2x2 m )
        {
            float x = m_v[0].x, y = m_v[0].y;
            m_v[0].x = x*m[0].x + y*m[1].x;
            m_v[0].y = x*m[0].y + y*m[1].y;
            x = m_v[1].x, y = m_v[1].y;
            m_v[1].x = x*m[0].x + y*m[1].x;
            m_v[1].y = x*m[0].y + y*m[1].y;
            return *this;
        }
        INLINE void Transpose( )
        {
            Swap< float >( m_v[0].y, m_v[1].x );
        }
        INLINE float Determinant( ) const
        {
            return m_v[0].WedgeProduct( m_v[1] );
        }
        INLINE float Trace( void ) const
        {
            return m_v[0].x + m_v[1].y;
        }
        INLINE void Identity( )
        {
            m_v[0].x = 1.f;
            m_v[1].y = 1.f;
        }
        INLINE void Inverse( )
        {
            float d = Determinant( );
            if( d > MAT_EPSILON ) {
                d = 1.f / d;
                Swap< float >( m_v[0].x, m_v[1].y );
                m_v[0].y = -m_v[0].y;
                m_v[1].x = -m_v[1].x;
                *this *= d;
            }
        }
        INLINE void Zero( )
        {
            memset( m_v, 0, sizeof( m_v ) );
        }
        INLINE int GetDimension( )
        {
            return 2;
        }

    private:

        Vec2 m_v[2];
    };

    class Mat3x3
    {

    public:

        INLINE Mat3x3( )
        {
            Zero( );
        }
        explicit INLINE Mat3x3( const float xx, const float xy, const float xz,
                                const float yx, const float yy, const float yz,
                                const float zx, const float zy, const float zz )
        {
            m_v[0].x = xx;
            m_v[0].y = xy;
            m_v[0].z = xz;
            m_v[1].x = yx;
            m_v[1].y = yy;
            m_v[1].z = yz;
            m_v[2].x = zx;
            m_v[2].y = zy;
            m_v[2].z = zz;
        }
	    // returns row!
        INLINE const Vec3& operator[]( int index ) const
        {
            return m_v[index];
        }
	    // returns row!
        INLINE Vec3& operator[]( int index )
        {
            return m_v[index];
        }
        INLINE Mat3x3 operator+( const Mat3x3 &m ) const
        {
            return Mat3x3( m_v[0].x+m[0].x, m_v[0].y+m[0].y, m_v[0].z+m[0].z,
                           m_v[1].x+m[1].x, m_v[1].y+m[1].y, m_v[1].z+m[1].z,
                           m_v[2].x+m[2].x, m_v[2].y+m[2].y, m_v[2].z+m[2].z );
        }
        INLINE Mat3x3 operator-( const Mat3x3 &m ) const
        {
            return Mat3x3( m_v[0].x-m[0].x, m_v[0].y-m[0].y, m_v[0].z-m[0].z,
                           m_v[1].x-m[1].x, m_v[1].y-m[1].y, m_v[1].z-m[1].z,
                           m_v[2].x-m[2].x, m_v[2].y-m[2].y, m_v[2].z-m[2].z );
        }
        INLINE Mat3x3 operator*( float a ) const
        {
            return Mat3x3( m_v[0].x*a, m_v[0].y*a, m_v[0].z*a,
                           m_v[1].x*a, m_v[1].y*a, m_v[1].z*a,
                           m_v[2].x*a, m_v[2].y*a, m_v[2].z*a );
        }
        INLINE Mat3x3 operator*( const Mat3x3 &m ) const
        {
            Mat3x3 dst;
            const float *l = reinterpret_cast<const float *>( this ),
                        *r = reinterpret_cast<const float *>( &m );
            for( int i = 0; i < 3; i++ ) {
                for( int j = 0; j < 3; j++ ) {
                    dst[i][j] = l[0] * r[j] + l[1] * r[3 + j] + l[2] * r[2*3 + j];
                }
                l += 3;
            }
            return dst;
        }
        INLINE Vec3 operator*( const Vec3 &v ) const
        {
            return Vec3( v.x*m_v[0].x + v.y*m_v[1].x + v.z*m_v[2].x,
                         v.x*m_v[0].y + v.y*m_v[1].y + v.z*m_v[2].y,
                         v.x*m_v[0].z + v.y*m_v[1].z + v.z*m_v[2].z );
        }
        INLINE Mat3x3 operator-( ) const
        {
            return Mat3x3( -m_v[0].x, -m_v[0].y, -m_v[0].z,
                           -m_v[1].x, -m_v[1].y, -m_v[1].z,
                           -m_v[2].x, -m_v[2].y, -m_v[2].z );
        }
        INLINE Mat3x3& operator+=( const Mat3x3 &m )
        {
            m_v[0].x += m[0].x;
            m_v[0].y += m[0].y;
            m_v[0].z += m[0].z;
            m_v[1].x += m[1].x;
            m_v[1].y += m[1].y;
            m_v[1].z += m[1].z;
            m_v[2].x += m[2].x;
            m_v[2].y += m[2].y;
            m_v[2].z += m[2].z;
            return *this;
        }
        INLINE Mat3x3& operator-=( const Mat3x3 &m )
        {
            m_v[0].x -= m[0].x;
            m_v[0].y -= m[0].y;
            m_v[0].z -= m[0].z;
            m_v[1].x -= m[1].x;
            m_v[1].y -= m[1].y;
            m_v[1].z -= m[1].z;
            m_v[2].x -= m[2].x;
            m_v[2].y -= m[2].y;
            m_v[2].z -= m[2].z;
            return *this;
        }
        INLINE Mat3x3& operator*=( float a )
        {
            m_v[0].x *= a;
            m_v[0].y *= a;
            m_v[0].z *= a;
            m_v[1].x *= a;
            m_v[1].y *= a;
            m_v[1].z *= a;
            m_v[2].x *= a;
            m_v[2].y *= a;
            m_v[2].z *= a;
            return *this;
        }
        INLINE Mat3x3& operator*=( Mat3x3 m )
        {
            float *l = reinterpret_cast<float *>( this ), dst[3];
            const float *r = reinterpret_cast<const float *>( &m );
            for( int i = 0; i < 3; i++ ) {
                for( int j = 0; j < 3; j++ ) {
                    dst[j] = l[0] * r[j] + l[1] * r[3 + j] + l[2] * r[2*3 + j];
                }
                l[0] = dst[0], l[1] = dst[1], l[2] = dst[2];
                l += 3;
            }
            return *this;
        }
        INLINE void Transpose( )
        {
            Swap< float >( m_v[0].y, m_v[1].x );
            Swap< float >( m_v[0].z, m_v[2].x );
            Swap< float >( m_v[1].z, m_v[2].y );
        }
        INLINE float Determinant( ) const
        {
            return m_v[1].CrossProduct( m_v[2] ).DotProduct( m_v[0] );
        }
        INLINE float Trace( ) const
        {
            return m_v[0].x + m_v[1].y + m_v[2].z;
        }
        INLINE void Identity( )
        {
            m_v[0].x = 1.f;
            m_v[1].y = 1.f;
            m_v[2].z = 1.f;
        }
        INLINE void Inverse( )
        {
            float d = Determinant( );
            if( d > MAT_EPSILON ) {
                d = 1.f / d;
                Swap< float >( m_v[0].x, m_v[1].y );
                m_v[0].y = -m_v[0].y;
                m_v[1].x = -m_v[1].x;
                *this *= d;
            }
        }
        INLINE void Zero( )
        {
            memset( m_v, 0, sizeof( m_v ) );
        }
        INLINE int GetDimension( )
        {
            return 3;
        }

    private:

        Vec3 m_v[3];
    };

    class Mat4x4
    {

    public:

        INLINE Mat4x4( void )
        {
            Zero( );
        }
        explicit INLINE Mat4x4( const float xx, const float xy, const float xz, const float xw,
                                const float yx, const float yy, const float yz, const float yw,
                                const float zx, const float zy, const float zz, const float zw,
                                const float wx, const float wy, const float wz, const float ww )
        {
            m_v[0].x = xx;
            m_v[0].y = xy;
            m_v[0].z = xz;
            m_v[0].w = xw;
            m_v[1].x = yx;
            m_v[1].y = yy;
            m_v[1].z = yz;
            m_v[1].w = yw;
            m_v[2].x = zx;
            m_v[2].y = zy;
            m_v[2].z = zz;
            m_v[2].w = zw;
            m_v[3].x = wx;
            m_v[3].y = wy;
            m_v[3].z = wz;
            m_v[3].w = ww;
        }
        INLINE void GetData( float out[16], bool isRowMajor = false ) const
        {
            if( isRowMajor ) {
                m_v[0].GetData( out );
                m_v[1].GetData( out + 4 );
                m_v[2].GetData( out + 8 );
                m_v[3].GetData( out + 12 );
            } else {
                for( int i = 0; i < 4; i++ ) {
                    out[i]      = m_v[i].x;
                    out[4 + i]  = m_v[i].y;
                    out[8 + i]  = m_v[i].z;
                    out[12 + i] = m_v[i].w;
                }
            }
        }
	    // returns row!
        INLINE const Vec4& operator[]( int index ) const
        {
            return m_v[index];
        }
	    // returns row!
        INLINE Vec4& operator[]( int index )
        {
            return m_v[index];
        }
        INLINE Mat4x4 operator+( const Mat4x4 &m ) const
        {
            return Mat4x4( m_v[0].x+m[0].x, m_v[0].y+m[0].y, m_v[0].z+m[0].z, m_v[0].w+m[0].w,
                           m_v[1].x+m[1].x, m_v[1].y+m[1].y, m_v[1].z+m[1].z, m_v[1].w+m[1].w,
                           m_v[2].x+m[2].x, m_v[2].y+m[2].y, m_v[2].z+m[2].z, m_v[2].w+m[2].w,
                           m_v[3].x+m[3].x, m_v[3].y+m[3].y, m_v[3].z+m[3].z, m_v[3].w+m[3].w );
        }
        INLINE Mat4x4 operator-( const Mat4x4 &m ) const
        {
            return Mat4x4( m_v[0].x-m[0].x, m_v[0].y-m[0].y, m_v[0].z-m[0].z, m_v[0].w-m[0].w,
                           m_v[1].x-m[1].x, m_v[1].y-m[1].y, m_v[1].z-m[1].z, m_v[1].w-m[1].w,
                           m_v[2].x-m[2].x, m_v[2].y-m[2].y, m_v[2].z-m[2].z, m_v[2].w-m[2].w,
                           m_v[3].x-m[3].x, m_v[3].y-m[3].y, m_v[3].z-m[3].z, m_v[3].w-m[3].w );
        }
        INLINE Mat4x4 operator*( float a ) const
        {
            return Mat4x4( m_v[0].x*a, m_v[0].y*a, m_v[0].z*a, m_v[0].w*a,
                           m_v[1].x*a, m_v[1].y*a, m_v[1].z*a, m_v[1].w*a,
                           m_v[2].x*a, m_v[2].y*a, m_v[2].z*a, m_v[2].w*a,
                           m_v[3].x*a, m_v[3].y*a, m_v[3].z*a, m_v[3].w*a );
        }
        INLINE Mat4x4 operator*( const Mat4x4 &m ) const
        {
            Mat4x4 dst;
            const float *l = reinterpret_cast<const float *>( this ),
                        *r = reinterpret_cast<const float *>( &m );
            for( int i = 0; i < 4; i++ ) {
                for( int j = 0; j < 4; j++ ) {
                    dst[i][j] = l[0] * r[j] + l[1] * r[4 + j] + l[2] * r[2*4 + j] + l[3] * r[3*4 + j];
                }
                l += 4;
            }
            return dst;
        }
        INLINE Vec4 operator*( const Vec4 &v ) const
        {
            return Vec4( v.x*m_v[0].x + v.y*m_v[1].x + v.z*m_v[2].x + v.w*m_v[3].x,
                         v.x*m_v[0].y + v.y*m_v[1].y + v.z*m_v[2].y + v.w*m_v[3].y,
                         v.x*m_v[0].z + v.y*m_v[1].z + v.z*m_v[2].z + v.w*m_v[3].z,
                         v.x*m_v[0].w + v.y*m_v[1].w + v.z*m_v[2].w + v.w*m_v[3].w );
        }
        INLINE Mat4x4 operator-( ) const
        {
            return Mat4x4( -m_v[0].x, -m_v[0].y, -m_v[0].z, -m_v[0].w,
                           -m_v[1].x, -m_v[1].y, -m_v[1].z, -m_v[1].w,
                           -m_v[2].x, -m_v[2].y, -m_v[2].z, -m_v[2].w,
                           -m_v[3].x, -m_v[3].y, -m_v[3].z, -m_v[3].w );
        }
        INLINE Mat4x4& operator+=( const Mat4x4 &m )
        {
            m_v[0].x += m[0].x;
            m_v[0].y += m[0].y;
            m_v[0].z += m[0].z;
            m_v[0].w += m[0].w;
            m_v[1].x += m[1].x;
            m_v[1].y += m[1].y;
            m_v[1].z += m[1].z;
            m_v[1].w += m[1].w;
            m_v[2].x += m[2].x;
            m_v[2].y += m[2].y;
            m_v[2].z += m[2].z;
            m_v[2].w += m[2].w;
            m_v[3].x += m[3].x;
            m_v[3].y += m[3].y;
            m_v[3].z += m[3].z;
            m_v[3].w += m[3].w;
            return *this;
        }
        INLINE Mat4x4& operator-=( const Mat4x4 &m )
        {
            m_v[0].x -= m[0].x;
            m_v[0].y -= m[0].y;
            m_v[0].z -= m[0].z;
            m_v[0].w -= m[0].w;
            m_v[1].x -= m[1].x;
            m_v[1].y -= m[1].y;
            m_v[1].z -= m[1].z;
            m_v[1].w -= m[1].w;
            m_v[2].x -= m[2].x;
            m_v[2].y -= m[2].y;
            m_v[2].z -= m[2].z;
            m_v[2].w -= m[2].w;
            m_v[3].x -= m[3].x;
            m_v[3].y -= m[3].y;
            m_v[3].z -= m[3].z;
            m_v[3].w -= m[3].w;
            return *this;
        }
        INLINE Mat4x4& operator*=( float a )
        {
            m_v[0].x *= a;
            m_v[0].y *= a;
            m_v[0].z *= a;
            m_v[0].w *= a;
            m_v[1].x *= a;
            m_v[1].y *= a;
            m_v[1].z *= a;
            m_v[1].w *= a;
            m_v[2].x *= a;
            m_v[2].y *= a;
            m_v[2].z *= a;
            m_v[2].w *= a;
            m_v[3].x *= a;
            m_v[3].y *= a;
            m_v[3].z *= a;
            m_v[3].w *= a;
            return *this;
        }
        INLINE Mat4x4& operator*=( Mat4x4 m )
        {
            float *l = reinterpret_cast<float *>( this ), dst[4];
            const float *r = reinterpret_cast<const float *>( &m );
            for( int i = 0; i < 4; i++ ) {
                for( int j = 0; j < 4; j++ ) {
                    dst[j] = l[0] * r[j] + l[1] * r[4 + j] + l[2] * r[2*4 + j] + l[3] * r[3*4 + j];
                }
                l[0] = dst[0], l[1] = dst[1], l[2] = dst[2], l[3] = dst[3];
                l += 4;
            }
            return *this;
        }
        INLINE void Transpose( )
        {
            Swap< float >( m_v[0].y, m_v[1].x );
            Swap< float >( m_v[0].z, m_v[2].x );
            Swap< float >( m_v[0].w, m_v[3].x );
            Swap< float >( m_v[1].z, m_v[2].y );
            Swap< float >( m_v[1].w, m_v[3].y );
            Swap< float >( m_v[2].w, m_v[3].z );
        }
        INLINE Mat4x4 GetTranspose( ) const
        {
            return Mat4x4( m_v[0].x, m_v[1].x, m_v[2].x, m_v[3].x,
                           m_v[0].y, m_v[1].y, m_v[2].y, m_v[3].y,
                           m_v[0].z, m_v[1].z, m_v[2].z, m_v[3].z,
                           m_v[0].w, m_v[1].w, m_v[2].w, m_v[3].w );
        }
        float Determinant( void ) const;
        float DeterminantSlow( void ) const;
        INLINE float Trace( void ) const
        {
            return m_v[0].x + m_v[1].y + m_v[2].z + m_v[3].w;
        }
        INLINE void Identity( )
        {
            m_v[0].x = 1.f;
            m_v[1].y = 1.f;
            m_v[2].z = 1.f;
            m_v[3].w = 1.f;
        }
        INLINE void Inverse( )
        {
        }
        INLINE void Zero( )
        {
            memset( m_v, 0, sizeof( m_v ) );
        }
        INLINE int GetDimension( ) const
        {
            return 4;
        }

        INLINE static Mat4x4 ScalarMatrix( float a )
        {
            return Mat4x4( a  , 0.f, 0.f, 0.f,
                           0.f, a  , 0.f, 0.f,
                           0.f, 0.f, a  , 0.f,
                           0.f, 0.f, 0.f, a   );
        }

        INLINE static Mat4x4 Translation( Vec3 dv )
        {
            return Mat4x4( 1.f, 0.f, 0.f, dv.x,
                           0.f, 1.f, 0.f, dv.y,
                           0.f, 0.f, 1.f, dv.z,
                           0.f, 0.f, 0.f, 1.f  );
        }
        INLINE static Mat4x4 Scaling( Vec3 d )
        {
            return Mat4x4( d.x, 0.f, 0.f, 0.0,
                           0.f, d.y, 0.f, 0.0,
                           0.f, 0.f, d.z, 0.0,
                           0.f, 0.f, 0.f, 1.f  ); 
        }
        INLINE static Mat4x4 RotationX( float da )
        {
            return Mat4x4( 1.f, 0.f,              0.f,             0.f,
                           0.f, Math::Cos( da ), -Math::Sin( da ), 0.f,
                           0.f, Math::Sin( da ),  Math::Cos( da ), 0.f,
                           0.f, 0.f,              0.f,             1.f );
        }
        INLINE static Mat4x4 RotationY( float da )
        {
            return Mat4x4(  Math::Cos( da ), 0.f, Math::Sin( da ), 0.f,
                            0.f,             1.f, 0.f,             0.f,
                           -Math::Sin( da ), 0.f, Math::Cos( da ), 0.f,
                            0.f,             0.f, 0.f,             1.f );
        }
        INLINE static Mat4x4 RotationZ( float da )
        {
            return Mat4x4( Math::Cos( da ), -Math::Sin( da ), 0.f, 0.f,
                           Math::Sin( da ),  Math::Cos( da ), 0.f, 0.f,
                           0.f,              0.f,             1.f, 0.f,
                           0.f,              0.f,             0.f, 1.f );
        }

    private:

        Vec4 m_v[4];
    };
}