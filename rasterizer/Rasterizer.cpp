﻿#include "precompiled.h"
#include "Rasterizer.h"

namespace Rasterizer2
{
    namespace {
        void APIENTRY GLDebugCallback( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char *message, void *userParam )
        {
            DebugOutput::Printf( "%s\n", message );
        }
        const char *modeValidValues[] = {"Windowed", "WindowedBorderless", "Fullscreen"};
    }

    Rasterizer::Rasterizer( IState *state ) :
        m_state( state ),
        m_viewportWidth( StateVar( "ViewportWidth", "Width of the viewport", 1024, StateVar::F_READABLE ) ),
        m_viewportHeight( StateVar( "ViewportHeight", "Height of the viewport", 768, StateVar::F_READABLE ) ),
        m_mode( StateVar( "Mode", "Controls graphics mode", "Windowed", StateVar::F_READABLE | StateVar::F_WRITABLE, modeValidValues, 3 ) ),
        m_vsync( StateVar( "VSync", "Turn vertical synchronization on/off", 0, StateVar::F_READABLE | StateVar::F_WRITABLE, nullptr, 0, 1 ) )
    {
    }

    Rasterizer::~Rasterizer( )
    {
    }

    bool Rasterizer::Initialize( )
    {
        int width = m_viewportWidth.GetIntValue( );
        int height = m_viewportHeight.GetIntValue( );

        if( SDL_Init( SDL_INIT_EVERYTHING ) != 0 ) {
            return false;
        }
        if( SDL_GL_LoadLibrary( GL_LIBRARY ) ) {
            return false;
        }

        SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
        SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
        SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
        SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 3 );
        //SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 8 );
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 );
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG );

        if( !( m_wnd = SDL_CreateWindow( "rasterizer", 0, 0, width, height, SDL_WINDOW_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_WINDOW_HIDDEN ) ) )
            return false;
        if( !strcmp( m_mode.GetStringValue( ), "Fullscreen" ) ) {
            SDL_SetWindowFullscreen( m_wnd, SDL_WINDOW_FULLSCREEN );
        } else if( !strcmp( m_mode.GetStringValue( ), "WindowedBorderless" ) ) {
            SDL_SetWindowFullscreen( m_wnd, SDL_WINDOW_FULLSCREEN_DESKTOP );
        } else {
            SDL_SetWindowFullscreen( m_wnd, 0 );
        }
        SDL_ShowWindow( m_wnd );
        
        if( !( m_context = SDL_GL_CreateContext( m_wnd ) ) ) {
            return false;
        }
        /*if( SDL_GL_MakeCurrent( m_wnd, m_context ) < 0 )
            return false;*/

        InitializeOpenGL( );
#ifdef _DEBUG
        PFNGLDEBUGMESSAGECALLBACKARBPROC _glDebugMessageCallbackARB = ( PFNGLDEBUGMESSAGECALLBACKARBPROC )SDL_GL_GetProcAddress( "glDebugMessageCallbackARB" );
        _glDebugMessageCallbackARB( GLDebugCallback, NULL );
        _glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB );
#endif

        // setup some stuff
        ilInit( );
        _glViewport( 0, 0, width, height );
        _glEnable( GL_DEPTH_TEST );
        _glEnable( GL_CULL_FACE );
        _glClearColor( 0.f, 0.f, 0.f, 0.f );
        _glClear( GL_COLOR_BUFFER_BIT );
        _glClear( GL_DEPTH_BUFFER_BIT );
        SDL_GL_SetSwapInterval( m_vsync.GetIntValue( ) );

        isMouseCaptured = false;

        return true;
    }

    void Rasterizer::SetupCommandSystem( ICommandSystem *cmdSystem )
    {
        const int STATE_VARS_CNT = 4;
        StateVar *stateVars[STATE_VARS_CNT] = {
            &m_viewportWidth,
            &m_viewportHeight,
            &m_mode,
            &m_vsync
        };
        int registeredVarsCnt = cmdSystem->RegisterStateVars( stateVars, STATE_VARS_CNT );
        DebugOutput::Printf( "Rasterizer::SetupCommandSystem: registered %d state vars\n", registeredVarsCnt );
    }

    // sequence: front - back - left - right - top - bottom, vertices go clockwise
    Mesh *Rasterizer::CreateCube( float edgeLength, Material front, Material back, Material left, Material right, Material top, Material bottom )
    {
        Vertex v[24];
        Mesh *mesh = m_state->GetMeshesManager( ).Create( );

        v[0].pos = Vec3( -0.5f, -0.5f, 0.5f );
        v[1].pos = Vec3( -0.5f, 0.5f, 0.5f );
        v[2].pos = Vec3( 0.5f, 0.5f, 0.5f );
        v[3].pos = Vec3( 0.5f, -0.5f, 0.5f );
        v[4].pos = Vec3( -0.5f, -0.5f, -0.5f );
        v[5].pos = Vec3( -0.5f, 0.5f, -0.5f );
        v[6].pos = Vec3( 0.5f, 0.5f, -0.5f );
        v[7].pos = Vec3( 0.5f, -0.5f, -0.5f );
        v[8].pos = v[4].pos;
        v[9].pos = v[5].pos;
        v[10].pos = v[1].pos;
        v[11].pos = v[0].pos;
        v[12].pos = v[7].pos;
        v[13].pos = v[6].pos;
        v[14].pos = v[2].pos;
        v[15].pos = v[3].pos;
        v[16].pos = v[1].pos;
        v[17].pos = v[5].pos;
        v[18].pos = v[6].pos;
        v[19].pos = v[2].pos;
        v[20].pos = v[0].pos;
        v[21].pos = v[4].pos;
        v[22].pos = v[7].pos;
        v[23].pos = v[3].pos;

        v[0].uv = Vec2( 0.f, 0.f );
        v[1].uv = Vec2( 0.f, 1.f );
        v[2].uv = Vec2( 1.f, 1.f );
        v[3].uv = Vec2( 1.f, 0.f );
        for( int i = 4; i < 24; i++ ) {
            v[i].uv = v[i%4].uv;
        }

        for( int i = 0; i < 24; i++ ) {
            Vec4 pos = Mat4x4::Scaling( Vec3( edgeLength, edgeLength, edgeLength ) ) * Vec4( v[i].pos );
            v[i].pos.x = pos.x;
            v[i].pos.y = pos.y;
            v[i].pos.z = pos.z;
        }

        mesh->SetVertexData( v, 24 );

        Material materials[] = { front, back, left, right, top, bottom };
        int indexes[] = { 0, 1, 2, 0, 3, 2, 4, 5, 6, 4, 7, 6, 8, 9, 10, 8, 11, 10, 12, 13, 14, 12, 15, 14, 16, 17, 18, 16, 19, 18, 20, 21, 22, 20, 23, 22 };
        int groupBorders[] = { 5, 11, 17, 23, 29, 35 };
        mesh->SetIndexData( indexes, 36, materials, 6, groupBorders );

        return mesh;
    }

    Mesh *Rasterizer::CreateQuad( )
    {
        Vertex v[4];
        Mesh *mesh = m_state->GetMeshesManager( ).Create( );

        v[0].pos = Vec3( -1.f, -1.f, -1.f );
        v[1].pos = Vec3( -1.f, 1.f, -1.f );
        v[2].pos = Vec3( 1.f, 1.f, -1.f );
        v[3].pos = Vec3( 1.f, -1.f, -1.f );
        mesh->SetVertexData( v, 4 );

        Material material;
        int indexes[] = { 0, 1, 2, 0, 3, 2 };
        int groupBorders = 5;
        mesh->SetIndexData( indexes, 6, &material, 1, &groupBorders );

        return mesh;
    }

    void Rasterizer::Update( )
    {
        SDL_PumpEvents( );

        keyboard = ( unsigned char* )SDL_GetKeyboardState( &numkeys );
        mouse = SDL_GetMouseState( &mouse_x, &mouse_y );
        wndFlags = SDL_GetWindowFlags( m_wnd );

        // if mouse is not captured or window is not in focus ignore mouse activity
        if( isMouseCaptured && ( wndFlags & SDL_WINDOW_INPUT_FOCUS ) ) {
            SDL_WarpMouseInWindow( m_wnd, m_viewportWidth.GetIntValue( ) / 2, m_viewportHeight.GetIntValue( ) / 2 );
        }
    }

    void Rasterizer::MouseCapture( )
    {
        isMouseCaptured = true;
        SDL_ShowCursor( SDL_DISABLE );
        SDL_WarpMouseInWindow( m_wnd, m_viewportWidth.GetIntValue( ) / 2, m_viewportHeight.GetIntValue( ) / 2 );
    }

    void Rasterizer::MouseRelease( )
    {
        isMouseCaptured = false;
        SDL_ShowCursor( SDL_ENABLE );
    }

    void Rasterizer::Frame( )
    {
        SDL_GL_SwapWindow( m_wnd );
        _glClear( GL_COLOR_BUFFER_BIT );
        _glClear( GL_DEPTH_BUFFER_BIT );
    }

}
