#include "precompiled.h"

namespace Rasterizer2
{
    const float Math::PI = 3.14159265358979323846f;
    const float Math::E  = 2.71828182845904523536f;
}