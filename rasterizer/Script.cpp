#include "precompiled.h"
#include "Script.h"

namespace Rasterizer2
{

    Script::Script( const char *name, ICommandSystem *cmdSystem, ResourceHolder< Script > &scriptsManager ) : m_scriptsManager( scriptsManager ), m_cmdSystem( cmdSystem )
    {
        if( name ) {
            strncpy( m_name, name, SCRIPT_NAME_MAX_LENGTH );
        } else {
            m_name[0] = 0;
        }

        m_stringPool.Initialize( MAX_SCRIPT_SIZE + 1 ); // TODO: add granularity to MemoryPool and take it into account here
        m_commandVectorsPool.Initialize( sizeof( std::vector< Command > ) * COMMAND_VECTORS_CNT );
    }

    void Script::State::SetIgnoring( const char **delims, int delimCount )
    {
        parseFlags &= IGNORING;
        for( int i = 0; i < delimCount && i < IGNORE_DELIM_COUNT; ++i ) {
            for( int j = 0; j < CHAR_BUF_SIZE; ++j ) {
                ignoreDelims[i][j] = delims[i][j];
            }
        }
        ignoreCharBuf = { 0, 0, 0 };
        ignoreDelimsCnt = delimCount;
    }

    void Script::State::ClearIgnoring( )
    {
        parseFlags &= ~IGNORING;
        ignoreDelimsCnt = 0;
    }

    void Script::State::UpdateCharBuf( char *buf, char c )
    {
        for( int i = 0; i < CHAR_BUF_SIZE - 1; ++i ) {
            buf[i] = buf[i+1];
        }

        buf[CHAR_BUF_SIZE - 1] = c;
    }

    void Script::State::UpdateLineCol( )
    {
        if( charBuf[1] == '\n' ) {
            ++line;
            col = 1;
        } else {
            ++col;
        }
    }

    const char *Script::State::CheckDelims( const char *buf, const char **delims, int delimCount ) const
    {
        for( int i = 0; i < delimCount; ++i ) {
            bool found = true;
            for( int j = 0; j < CHAR_BUF_SIZE; ++j ) {
                if( buf[j] != delims[i][j] && ( j == CHAR_BUF_SIZE - 1 || delims[i][j] ) ) {
                    found = false;
                }
            }
            if( found ) {
                return delims[i];
            }
        }

        return nullptr;
    }

    bool Script::State::TestBuf( const char *buf, const char *testSeq ) const
    {
        for( int i = 0; i < CHAR_BUF_SIZE; ++i ) {
            if( buf[i] != testSeq[i] && ( i == CHAR_BUF_SIZE - 1 || testSeq[i] ) ) {
                return false;
            }
        }

        return true;
    }

    bool Script::Parse( const char *text )
    {
        m_commands.clear( );
        m_commands.reserve( RESERVED_COMMANDS_CNT );
        m_stateStack.clear( );

        State state;

        m_stateStack.push_back( SemanticState( ) );

        while( *text ) {
            if( state.parseFlags & IGNORING ) {
                state.UpdateCharBuf( state.ignoreCharBuf, *text++ );

                const char *foundDelim = state.CheckDelims( state.ignoreCharBuf, state.ignoreDelims, state.ignoreDelimsCnt );
                if( foundDelim ) {
                    state.ClearIgnoring( );
                }

                state.UpdateLineCol( );

                continue;
            }

            state.UpdateCharBuf( state.charBuf, *text++ );

            if( state.TestBuf( state.charBuf, "\0//" ) ) {
                // double slash comments

                const char *delims[] = { "\0\0\n" };
                state.SetIgnoring( delims, 2 );
                state.AccumBufPop( );
            } else if( state.TestBuf( state.charBuf, "\0/*" ) ) {
                // star slash comments

                const char *delims[] = { "\0*/" };
                state.SetIgnoring( delims, 1 );
                state.AccumBufPop( );
            } else if( state.state & ACCUM_TOKEN ) {
                // end of token delims
                if( state.CheckDelims( state.charBuf, { "\0\0 ", "\0\0\n", "\0\0;" }, 3 ) ) {
                    ProcessToken( state );
                }

                if( state.CheckDelims( state.charBuf, { "\0\n\"", "\0;\"", "\0 \"" }, delimCnt ) ) { 
                    // opening double quotes

                    state.state &= ~ACCUM_TOKEN;
                    state.state &= ACCUM_DOUBLE_QUOTED_TOKEN;
                } else if( state.CurChar( ) == '\"' ) {
                    // wrong double quotes position
                    PrintError( state, "Opening double quotes(\") must be preceded by \'\\n\' or \';\' or \' \'" );
                } else if( state.CheckDelims( state.charBuf, { "=" }, 1 ) ) {
                    // named declaration token
                   if( state.accumBufIndex > 0 ) {
                       ProcessToken( state );
                   } else {
                       if( !blankToken || !blankToken[0] ) {
                           PrintError( state, "Named declaration must have a name" );
                       } else {
                           strcpy( state.accumBuf, blankToken );
                           ProcessToken( state );
                       }
                   }
                } else {
                    state.AccumBufPush( state.CurChar( ) );
                }
            } else if( state.state & ACCUM_DOUBLE_QUOTED_TOKEN ) {
                // double quotes end
                if( state.CheckDelims( state.charBuf, { "\0\0\"" }, 1 ) ) {
                    ProcessToken( state );
                }
            }

            state.UpdateLineCol( );
        }
    }

    void Script::ProcessToken( State &state )
    {
        state.AccumBufPush( 0 );
        char *token = state.accumBuf;

        // ignore empty tokens
        if( token && token[0] ) {
        }

        state.AccumBufReset( );
    }

    void Script::PrintError( const State &state, const char *msg, ... ) const
    {
        char msgBuf[DebugOutput::MAX_MESSAGE_LENGTH+1];

        va_list args;

        va_start( args, msg );
        vsprintf( msgBuf, msg, args );
        va_end( args );

        DebugOutput::Printf( "Script \"%s\" parsing error on line %d, col %d: \"%s\"", m_name, state.line, state.col, msgBuf );
    }

}