#include "precompiled.h"
#include "Image.h"

namespace Rasterizer2
{
    Image::~Image( )
    {
        ilDeleteImage( m_image );
    }

    bool Image::LoadArbitraryImage( const char *path )
    {
        if( strlen( path ) + strlen( IMAGE_LOCATION ) >= MAX_PATH ) {
            DebugOutput::Printf( "\"%s\" image file name is too long!\n", path );
            return false;
        }

        char fullPath[MAX_PATH];
        strcpy( fullPath, IMAGE_LOCATION );
        strcat( fullPath, path );

        m_image = ilGenImage( );
        ilBindImage( m_image );
        if( !ilLoadImage( fullPath ) ) {
            DebugOutput::Printf( "Failed to load: \"%s\"\n", path );
            return false;
        }
        m_width = ilGetInteger( IL_IMAGE_WIDTH );
        m_height = ilGetInteger( IL_IMAGE_HEIGHT );
        return true;
    }

    void Image::GetData( byte *outBuf, PixelFormat pf )
    {
        ilBindImage( m_image );

        int fmtBytes = GetPixelFormatSize( pf )/*, bytesCount = m_width * m_height * fmtBytes*/;
        ILenum format, type;
        switch( pf ) {
        case PIXEL_R8G8B8:
            format = IL_RGB;
            type = IL_UNSIGNED_BYTE;
            break;
        case PIXEL_R8G8B8A8:
            format = IL_RGBA;
            type = IL_UNSIGNED_BYTE;
            break;
        default:
            format = IL_RGB;
            type = IL_UNSIGNED_BYTE;
        }
        ilCopyPixels( 0, 0, 0, m_width, m_height, 1, format, type, outBuf );
        
        for( int i = 0; i < m_height / 2; i++ ) {
            for( int j = 0; j < m_width * fmtBytes; j++ ) {
                std::swap( outBuf[i * m_width * fmtBytes + j], outBuf[m_width * fmtBytes * ( m_height - i - 1 ) + j] );
            }
        }
    }

    int Image::GetPixelFormatSize( PixelFormat fmt )
    {
        switch( fmt ) {
        case PIXEL_R8G8B8:
            return 3;
        case PIXEL_R8G8B8A8:
            return 4;
        }
        return 0;
    }
}
