#include "precompiled.h"
#include "StateVar.h"

namespace Rasterizer2
{
    template< typename T, typename U = T >
    bool isValueValid( T value, std::vector< U > validValues )
    {
        if( validValues.size( ) > 0 ) {
            for( U validValue : validValues ) {
                if( validValue == value ) {
                    return true;
                }
            }
        } else {
            return true;
        }

        return false;
    }

    StateVar::StateVar( const char *name, const char *description, uint flags, Type type ) :
        m_flags( flags ), m_type( type ), m_intMinValue( std::numeric_limits<int>::min( ) ), m_intMaxValue( std::numeric_limits<int>::max( ) ),
        m_floatMinValue( std::numeric_limits<float>::min( ) ), m_floatMaxValue( std::numeric_limits<float>::max( ) )
    {
        m_name[MAX_NAME_LENGTH] = 0;
        m_description[MAX_DESCRIPTION_LENGTH] = 0;
        m_strValue[MAX_STRING_VALUE_LENGTH] = 0;
        m_defaultStrValue[MAX_STRING_VALUE_LENGTH] = 0;

        if( flags & F_CONSTANT ) {
            flags &= ~F_WRITABLE;
        }

        strncpy( m_name, name, MAX_NAME_LENGTH );
        strncpy( m_description, description, MAX_DESCRIPTION_LENGTH );
    }

    StateVar::StateVar( const char *name, const char *description, const char *strValue, uint flags, const char **validValues, int validValsCnt, Type type ) :
        StateVar( name, description, flags, type )
    {
        if( validValues ) {
            for( int i = 0; i < validValsCnt; ++i ) {
                m_strValidValues.push_back( std::string( validValues[i] ) );

                m_intValidValues.push_back( 0 );
                sscanf( validValues[i], "%d", &m_intValidValues.back( ) );
                m_floatValidValues.push_back( 0.f );
                sscanf( validValues[i], "%f", &m_floatValidValues.back( ) );
            }
        }

        SetStringValue( strValue );
        SetDefaults( );
    }

    StateVar::StateVar( const char *name, const char *description, int intValue, uint flags, int *validValues, int validValsCnt, int minValue, int maxValue ) :
        StateVar( name, description, flags, Type::INTEGER )
    {
        if( validValues ) {
            for( int i = 0; i < validValsCnt; ++i) {
                m_intValidValues.push_back( validValues[i] );
                m_floatValidValues.push_back( validValues[i] * 1.f );

                char strValidValue[MAX_STRING_VALUE_LENGTH + 1];
                snprintf( strValidValue, MAX_STRING_VALUE_LENGTH + 1, "%d" );
                m_strValidValues.push_back( std::string( strValidValue ) );
            }
        }

        m_intMinValue = minValue;
        m_intMaxValue = maxValue;
        m_floatMinValue = minValue * 1.f;
        m_floatMaxValue = maxValue * 1.f;

        SetIntValue( intValue );
        SetDefaults( );
    }

    StateVar::StateVar( const char *name, const char *description, float floatValue, uint flags, float *validValues, int validValsCnt, float minValue, float maxValue ) :
        StateVar( name, description, flags, Type::FLOAT )
    {
        if( validValues ) {
            for( int i = 0; i < validValsCnt; ++i) {
                m_floatValidValues.push_back( validValues[i] );
                m_intValidValues.push_back( ( int )validValues[i] );

                char strValidValue[MAX_STRING_VALUE_LENGTH + 1];
                snprintf( strValidValue, MAX_STRING_VALUE_LENGTH + 1, "%f" );
                m_strValidValues.push_back( std::string( strValidValue ) );
            }
        }

        m_floatMinValue = minValue;
        m_floatMaxValue = maxValue;
        m_intMinValue = ( int )minValue;
        m_intMaxValue = ( int )maxValue;

        SetFloatValue( floatValue );
        SetDefaults( );
    }

    void StateVar::SetStringValue( const char* strValue )
    {
        if( isValueValid<const char*, std::string>( strValue, m_strValidValues ) ) {
            strncpy( m_strValue, strValue, MAX_STRING_VALUE_LENGTH );
            sscanf( m_strValue, "%d", &m_intValue );
            sscanf( m_strValue, "%f", &m_floatValue );
        }
    }

    void StateVar::SetIntValue( int intValue )
    {
        if( isValueValid<int>( intValue, m_intValidValues ) && intValue >= m_intMinValue && intValue <= m_intMaxValue ) {
            snprintf( m_strValue, MAX_STRING_VALUE_LENGTH + 1, "%d" );
            m_intValue = intValue;
            m_floatValue = intValue * 1.f;
        }
    }

    void StateVar::SetFloatValue( float floatValue ) 
    {
        if( isValueValid<float>( floatValue, m_floatValidValues ) && floatValue >= m_floatMinValue && floatValue <= m_floatMaxValue ) {
            snprintf( m_strValue, MAX_STRING_VALUE_LENGTH + 1, "%f" );
            m_intValue = ( int )floatValue;
            m_floatValue = floatValue;
        }
    }

    void StateVar::SetDefaults( )
    {
        strcpy( m_defaultStrValue, m_strValue );
        m_defaultIntValue = m_intValue;
        m_defaultFloatValue = m_floatValue;
    }
}
