#pragma once

#include "File.h"
#include "CommandSystem.h"

namespace Rasterizer2
{
    class ScriptFile : public File
    {

    public:

        ScriptFile(  );
        ~ScriptFile( );

        void Clear( );

    private:

        struct Command
        {
        };
        struct Script
        {
        };

        bool Parse( FILE *file );
        bool ParseFromMemory( const void *buffer );

    };
}