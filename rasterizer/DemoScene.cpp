#include "precompiled.h"
#include "Image.h"
#include "DemoScene.h"
#include "OBJFile.h"

namespace Rasterizer2 
{
    DemoScene::DemoScene( Rasterizer &r, IState *state ) :
        m_r( r ),
        m_state( state ),
        m_vFOV( StateVar( "VerticalFOV", "Vertical Field of View in degrees", 90.f, StateVar::F_READABLE | StateVar::F_WRITABLE, nullptr, 0, 0.1f, 180.f ) ),
        m_hFOV( StateVar( "HorizontalFOV", "Horizontal Field of View in degrees", 105.f, StateVar::F_READABLE | StateVar::F_WRITABLE, nullptr, 0, 0.1f, 180.f ) ),
        m_near( StateVar( "NearClip", "Near clipping plane Z coordinate", 0.1f, StateVar::F_READABLE | StateVar::F_WRITABLE, nullptr, 0, 0.f ) ),
        m_far( StateVar( "FarClip", "Far clipping plane Z coordinate", 1000.f, StateVar::F_READABLE | StateVar::F_WRITABLE, nullptr, 0, 0.f ) ),
        m_simSpeed( StateVar( "SimulationSpeed", "Do simulation x times as fast", 1.f, StateVar::F_READABLE | StateVar::F_WRITABLE, nullptr, 0, 0.f, 1.f ) )
    {
    }

    DemoScene::~DemoScene( )
    {
    }

    bool DemoScene::Create( )
    {
        m_r.MouseCapture( );

        OBJFile model( m_state );
        const char *MODEL_NAME = "Scimiter.obj";
        double msec = Counter( );
        if( !model.LoadFile( MODEL_NAME ) ) {
            return false;
        }
        DebugOutput::Printf( "load time msec: %f\n", Counter( ) - msec );
        msec = Counter( );
        m_cube = model.MakeMesh( );
        if( !m_cube ) {
            return false;
        }
        DebugOutput::Printf( "create time msec: %f\n", Counter( ) - msec );

        return true;
    }

    void DemoScene::SetupCommandSystem( ICommandSystem *cmdSystem )
    {
        const int STATE_VARS_CNT = 5;
        StateVar *stateVars[STATE_VARS_CNT] = {
            &m_vFOV,
            &m_hFOV,
            &m_near,
            &m_far,
            &m_simSpeed
        };
        int registeredVarsCnt = cmdSystem->RegisterStateVars( stateVars, STATE_VARS_CNT );
        DebugOutput::Printf( "DemoScene::SetupCommandSystem: registered %d state vars\n", registeredVarsCnt );
    }

    bool DemoScene::Frame( float timeDelta )
    {
        m_r.view.SetPerspectiveFOV( m_hFOV.GetFloatValue( ), m_vFOV.GetFloatValue( ), -m_near.GetFloatValue( ), -m_far.GetFloatValue( ) );

        m_r.Update( );

        if( m_r.keyboard[SDL_SCANCODE_Q] ) {
            return false;
        }

        if( m_r.isMouseCaptured ) {
            const float velocity = 10.f * 0.001f, avelocity = 0.8f * Math::PI * 0.001f,
                sensitivity = 5.f;
            const int SCR_CENTERX = m_r.GetWindowWidth( ) / 2, SCR_CENTERY = m_r.GetWindowHeight( ) / 2;
            Mat3x3 viewVec;
            m_r.view.GetAngles( ).ToVectors( viewVec, true, true, true );
            Vec3 dv = viewVec[0], rv = viewVec[1], uv = viewVec[2];
            float lr = 1.f, rr = -1.f;
            if( uv.y < -0.8 ) {
                lr = -1.f;
                rr = 1.f;
            }

            if( m_r.keyboard[SDL_SCANCODE_UP] ) {
                m_r.view.Pitch( timeDelta * avelocity );
            }
            if( m_r.keyboard[SDL_SCANCODE_DOWN] ) {
                m_r.view.Pitch( -timeDelta * avelocity );
            }
            if( m_r.keyboard[SDL_SCANCODE_RIGHT] ) {
                m_r.view.Yaw( timeDelta * avelocity * rr );
            }
            if( m_r.keyboard[SDL_SCANCODE_LEFT] ) {
                m_r.view.Yaw( timeDelta * avelocity * lr );
            }

            Vec2 rotVec( ( float )( m_r.mouse_x - SCR_CENTERX ), ( float )( m_r.mouse_y - SCR_CENTERY ) );
            m_r.view.Pitch( sensitivity * rotVec.y * 0.0005f * -1.f );
            m_r.view.Yaw( sensitivity * rotVec.x * 0.0005f * -1.f * lr );

            if( m_r.keyboard[SDL_SCANCODE_W] ) {
                dv *= timeDelta * velocity;
            } else if( m_r.keyboard[SDL_SCANCODE_S] ) {
                dv *= -timeDelta * velocity;
            } else {
                dv.Zero( );
            }
            if( m_r.keyboard[SDL_SCANCODE_D] ) {
                rv *= timeDelta * velocity;
            } else if( m_r.keyboard[SDL_SCANCODE_A] ) {
                rv *= -timeDelta * velocity;
            } else {
                rv.Zero( );
            }
            m_r.view.Move( dv + rv );
        }

        if( m_cube ) {
            m_cube->Draw( m_r.view );
        }
        /*m_cube->SetTransform( Mat4x4::Translation( Vec3( 1.f, -1.f, 0.f ) ) );
        m_cube->Draw( m_r.view );
        m_cube->SetTransform( Mat4x4::Translation( Vec3( -1.f, -1.f, 0.f ) ) );
        m_cube->Draw( m_r.view );
        m_cube->SetTransform( Mat4x4::ScalarMatrix( 1.f ) );*/

        if( m_r.keyboard[SDL_SCANCODE_ESCAPE] ) {
            m_r.MouseRelease( );
        }
        if( ( m_r.mouse & SDL_BUTTON( SDL_BUTTON_LEFT ) ) && ( m_r.wndFlags & SDL_WINDOW_INPUT_FOCUS ) ) {
            m_r.MouseCapture( );
        }

        return true;
    }

    void DemoScene::UpdateScreen( )
    {
        m_r.Frame( );
    }
    }