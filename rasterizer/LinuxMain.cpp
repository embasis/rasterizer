#include "precompiled.h"
#include "CommandSystem.h"
#include "State.h"

using namespace Rasterizer2;

namespace {
    State state;
    CommandSystem cmdSystem;
}

int main( int, char** )
{
    srand( ( unsigned int )time( NULL ) );
    float lastTime = ( float )Counter( );
    float lastFps = lastTime;

    state.SetupCommandSystem( ( ICommandSystem* )&cmdSystem );

    if( !state.LaunchDemoScene( ) ) {
        return 0;
    }

    while( 1 ) {
        float curTime = ( float )Counter( );
        if( !state.Frame( curTime - lastTime ) ) {
            break;
        }
        state.UpdateScreen( );
        lastTime = curTime;
    }

    return 0;
}
