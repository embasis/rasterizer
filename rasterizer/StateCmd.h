#pragma once

#include "IState.h"

namespace Rasterizer2
{
    class ICommandSystem;
    class Script;

    class StateCmd
    {

    public:

        struct ArgDesc
        {
            INLINE ArgDesc( ) : internalType( INT ), displayType( INT )
            {
            }

            static const int MAX_NAME_LENGTH = 48;
            static const int MAX_DESCRIPTION_LENGTH = 256;

            enum Type
            {
                INT = 1,
                UINT,
                FLOAT,
                STRING,
                SCRIPT
            };

            enum SemanticType
            {
                D_INT = 1,
                D_UINT,
                D_FLOAT,
                D_STRING,
                D_SCRIPT
            };

            char name[MAX_NAME_LENGTH+1];
            char description[MAX_DESCRIPTION_LENGTH+1];
            Type internalType;
            SemanticType displayType;
        };

        union ArgBasic
        {
            int i;
            uint ui;
            float f;
            const char* char_ptr;
            const std::vector< Script::Command > *cmds_ptr;
        };

        static const int MAX_NAME_LENGTH = 64;
        static const int MAX_DESCRIPTION_LENGTH = 256;
        static const int MAX_ARG_COUNT = 12;

        typedef void ( *StateCmdFuncPtr ) ( ICommandSystem*, IState*, ArgBasic*, int );

        INLINE StateCmd( const char *name, const char *description, ArgDesc *argDescs, int argCount, StateCmdFuncPtr cmdFunc ) : m_cmdFunc( cmdFunc )
        {
            m_name[MAX_NAME_LENGTH] = 0;
            m_description[MAX_DESCRIPTION_LENGTH] = 0;

            strncpy( m_name, name, MAX_NAME_LENGTH );
            strncpy( m_description, description, MAX_DESCRIPTION_LENGTH );

            m_argCount = std::min( argCount, MAX_ARG_COUNT );
            memcpy( m_argDescs, argDescs, sizeof( ArgDesc ) * m_argCount );
        }

        INLINE const char *GetName( ) const
        {
            return m_name;
        }

        INLINE const char *GetDescription( ) const
        {
            return m_description;
        }

        INLINE const ArgDesc *GetArgDescs( ) const
        {
            return m_argDescs;
        }

        const StateCmd &operator()( ICommandSystem *cmdSystem, IState *state, int argCount, ... ) const;
        const StateCmd &operator()( ICommandSystem *cmdSystem, IState *state, ArgBasic *basicArgs, int basicArgsCnt ) const;

    private:

        char m_name[MAX_NAME_LENGTH+1];
        char m_description[MAX_DESCRIPTION_LENGTH+1];
        StateCmdFuncPtr m_cmdFunc;
        ArgDesc m_argDescs[MAX_ARG_COUNT];
        int m_argCount;

    };
}