#pragma once

namespace Rasterizer2
{
    class File
    {

    public:

        virtual ~File( );
        virtual void Clear( ) = 0;
        virtual bool LoadFile( const char *path );
        virtual bool LoadFileFromMemory( const char *name, const void *buffer );
        INLINE const char *GetPath( ) const
        {
            return m_path;
        }
        INLINE const char *GetFileName( ) const
        {
            return m_fileName;
        }

    protected:

        File( );

    private:

        char m_path[MAX_PATH];
        char m_fileName[MAX_PATH]; // file name without extension

        // specifies name of the derived class, that is printed into the debug output by LoadFile function
        virtual const char *GetFileType( ) const = 0;
        // path inside project directory where files of this type are stored
        virtual const char *GetFileLocation( ) const = 0;

        virtual bool Parse( FILE *file ) = 0;
        virtual bool ParseFromMemory( const void *buffer ) = 0;

    };
}