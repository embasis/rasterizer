#include "precompiled.h"
#include "View.h"

namespace Rasterizer2
{
    void View::SetOrtho( float left, float right, float bottom, float top, float near, float far )
    {
        m_projection.Identity( );
        m_projection[0].x = 2 / ( right - left );
        m_projection[1].y = 2 / ( top - bottom );
        m_projection[2].z = 2 / ( far - near );
        m_projection[0].w = - ( right + left ) / ( right - left );
        m_projection[1].w = - ( top + bottom ) / ( top - bottom );
        m_projection[2].w = - ( far + near ) / ( far - near );

        m_matrix = m_projection * m_view;
    }

    void View::SetPerspective( float left, float right, float bottom, float top, float near, float far )
    {
        far *= -1;
        near *= -1;
        m_projection.Zero( );
        m_projection[0].x = 2 * near / ( right - left );
        m_projection[1].y = 2 * near / ( top - bottom );
        m_projection[0].z = ( right + left ) / ( right - left );
        m_projection[1].z = ( top + bottom ) / ( top - bottom );
        m_projection[2].z = - ( far + near ) / ( far - near );
        m_projection[3].z = -1.f;
        m_projection[2].w = -2 * far * near / ( far - near );

        m_matrix = m_projection * m_view;
    }

    void View::SetPerspectiveFOV( float hfov, float vfov, float near, float far )
    {
        float hwidth  = near * Math::Tan( Math::DegToRadians( 0.5f * hfov ) ),
              hheight = near * Math::Tan( Math::DegToRadians( 0.5f * vfov ) );
        SetPerspective( hwidth, -hwidth, hheight, -hheight, near, far );
    }

    void View::UpdateMatrix( ) const
    {
        Angles adelta = m_viewAngles - m_oldAngles;
        Vec3 pdelta = m_pos - m_oldPos;
        if( adelta != Angles( 0.f, 0.f, 0.f ) || pdelta != Vec3( 0.f, 0.f, 0.f ) ) {
            Mat4x4 tr = Mat4x4::Translation( -pdelta );
            m_view = Mat4x4::RotationX( -m_viewAngles.pitch ) * Mat4x4::RotationY( -m_viewAngles.yaw ) * Mat4x4::RotationZ( m_viewAngles.roll ) * Mat4x4::Translation( -m_pos );
            m_matrix = m_projection * m_view;
        }
        m_oldAngles = m_viewAngles;
        m_oldPos = m_pos;
    }
}