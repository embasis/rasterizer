#pragma once

#include "Image.h"

namespace Rasterizer2
{

    class Texture
    {

    public:

        Texture( void );
        ~Texture( void );
            
        bool Create( byte *src, int width, int height, PixelFormat srcFmt, PixelFormat targetFmt, bool srgb );
        bool CreateFromImage( Image img, PixelFormat targetFmt, bool srgb );
        INLINE GLuint GetHandle( ) const
        {
            return m_texture;
        }

    private:

        GLuint m_texture;
        PixelFormat m_format;

    };

}