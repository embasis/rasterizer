#include "precompiled.h"
#include "Texture.h"

namespace Rasterizer2
{
    Texture::Texture( void ) : m_texture( 0 )
    {
    }

    Texture::~Texture( void )
    {
        _glDeleteTextures( 1, &m_texture );
    }

    bool Texture::Create( byte *src, int width, int height, PixelFormat srcFmt, PixelFormat targetFmt, bool srgb )
    {
        if( m_texture ) { 
            _glDeleteTextures( 1, &m_texture );
        }

        _glActiveTexture( GL_TEXTURE0 );
        _glGenTextures( 1, &m_texture );
        _glBindTexture( GL_TEXTURE_2D, m_texture );

        GLenum format = GL_RGB, type = GL_UNSIGNED_BYTE;
        switch( srcFmt ) {
        case PIXEL_R8G8B8:
            format = GL_RGB;
            type = GL_UNSIGNED_BYTE;
            break;
        case PIXEL_R8G8B8A8:
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        }

        GLint internalFormat = GL_RGB8;
        switch( targetFmt ) {
        case PIXEL_R8G8B8:
            if( srgb ) {
                internalFormat = GL_SRGB8;
            } else {
                internalFormat = GL_RGB8;
            }
            break;
        case PIXEL_R8G8B8A8:
            if( srgb ) {
                internalFormat = GL_SRGB8_ALPHA8;
            } else {
                internalFormat = GL_RGBA8;
            }
            break;
        }

        _glTexImage2D( GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, src );
        _glEnable( GL_TEXTURE_2D ); // ATI bugs? :D
        _glGenerateMipmap( GL_TEXTURE_2D );
        _glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        _glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        _glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
        _glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

        return true;
    }

    bool Texture::CreateFromImage( Image img, PixelFormat targetFmt, bool srgb )
    {
        int width = img.GetWidth( ), height = img.GetHeight( );
        byte *pixels = new byte[width * height * Image::GetPixelFormatSize( targetFmt )];
        img.GetData( pixels, targetFmt );
        
        bool res = Create( pixels, width, height, targetFmt, targetFmt, srgb );

        delete[] pixels;

        return res;
    }
}
